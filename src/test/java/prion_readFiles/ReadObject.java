package prion_readFiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

public class ReadObject {

	Properties p = new Properties();

	public Properties getObjectRepository() throws IOException {
		// Read OR file, create an object of FileInputStream class with the path to P file
		// way 1: InputStream stream = new FileInputStream(new File(System.getProperty("user.dir") + "\\src\\objects\\object_2.properties"));
		// way 2: ngan hon
		InputStream stream = new FileInputStream(
				System.getProperty("user.dir") + "\\src\\objectRepository_StoreWebElements\\object_2.properties");

		// Reading all objects from properties file can be done using load method offered by P class
		//p.load(stream);
		p.load(new InputStreamReader(stream, "UTF-8"));
		return p;
	}

}
