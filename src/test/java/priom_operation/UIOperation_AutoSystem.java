package priom_operation;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.jboss.aerogear.security.otp.Totp;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

//import prio1_card.card_Zing_Full;
import priop_rutgon.StringUtils;

public class UIOperation_AutoSystem extends priop_rutgon.rutgon {
	// JavascriptExecutor js = (JavascriptExecutor) driver;
	public UIOperation_AutoSystem(WebDriver driver) {
		super(driver);
	}

	public void perform(String operation, String objectName, String objectType, String key1, String key2, String key3,
			String key4, String key5, String value1, String value2, String value3, String value4, String value5,
			String value6, String value7, String value8, String value9, String value10, String value11, String value12,
			String value13, String value14, String value15, String value16, String value17, String value18,
			String value19, String value20, String step, String assertValue) throws Exception {

		long randLong = (long) (new Date().getTime());
		String rand = String.valueOf(randLong).substring(6);

		switch (operation.toUpperCase()) {

		case "CHANGE_ID":
			// System.out.println(step);
			try {
				wrapWaitSendKeysXpath("//input[@name='changeid']", "123456");
			} catch (Exception e) {
				System.out.println("Cannot find element to Send Keys");
			}
			break;

		case "CLICK":
			// System.out.println(step);
			try {
				wrapWaitClickXpath(value1);
			} catch (Exception e) {
				System.out.println("Cannot find element to Click");
			}
			break;

		case "CLICK_BY_JAVA":
			// System.out.println(step);
			try {
				wrapWaitClickXpathJava(value1);
			} catch (Exception e) {
				System.out.println("Cannot find element to Click by Java");
			}
			break;

		case "GET GOOGLE TOKEN":
			// System.out.println(step);
			try {
				String otpKeyStr = "6NXL EEI7 BZPG BXYR"; // <- this 2FA secret key.

				Totp totp = new Totp(otpKeyStr);
				String twoFactorCode = totp.now(); // <- got 2FA coed at this time!
				wrapWaitSendKeysXpath(value1, key1 + twoFactorCode);
				// System.out.println("twoFactorCode:" +twoFactorCode);
			} catch (Exception e) {
				System.out.println("cannot get Google Authenticator at this time");
			}
			break;

		case "IS_DISPLAYED":
			// System.out.println(step);
			try {
				WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(value1)));
				Assert.assertTrue(element.isDisplayed());
				System.out.println("Verify Element is displayed - correct");
			} catch (Exception e) {
				System.out.println("Cannot find element to check display");
			}
			break;//

		case "IS_NOT_DISPLAYED":
			// System.out.println(step);
			Boolean element1 = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(value1)));
			Assert.assertTrue(element1);
			System.out.println("Verify Element is not displayed - correct");
			break;//

		case "GET_TEXT":
			// System.out.println(step);
			try {
				WebElement element4 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(value1)));
				System.out.println(key1 + ": " + element4.getText());
			} catch (Exception e) {
				System.out.println("Cannot GET_TEXT");
			}
			break;//

		case "WAIT_TILL_ELEMENT_INVISIBLE":
			// System.out.println(step);
			Boolean element2 = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(value1)));
			Assert.assertTrue(element2);//
			break;

		case "NOT_CONTAIN_TEXT":
			// System.out.println(step);
			List<WebElement> lst2 = driver.findElements(By.xpath(value1));
			for (int k = 0; k < lst2.size(); k++) {
				lst2.get(k).getText();
				System.out.println(lst2.get(k).getText());
				if (lst2.get(k).getText().contains(key1)) {
					System.out.println("Text is contained");
				} else
					System.out.println("Text is not contained.");
			}

			Assert.assertTrue(driver.findElement(By.xpath(value1)).isDisplayed());
			break;

		case "NEW_ID_IS_CREATED":
			// System.out.println(step);
			String lastId = wrapWaitGetTextXpath(value1);
			System.out.println(lastId);
			break;

		case "CLEAR_DATA":
			// System.out.println(step);
			driver.findElement(By.xpath(value1)).clear();
			break;

		case "SENDKEY":
			// System.out.println(step);
			try {
				wrapWaitSendKeysXpath(value1, key1);
			} catch (Exception e) {
				System.out.println("Cannot find element to Send Keys");
			}
			// Thread.sleep(1000);
			break;

		/*
		 * case "DELETE_MANY": //System.out.println(step); try { List<WebElement> lstElm
		 * = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(
		 * value1))); if (lstElm.size() == 0) { break; } else { for (int k = 0; k <
		 * lstElm.size(); k++) { System.out.println(lstElm.size());
		 * lstElm.get(lstElm.size()-1).click(); // click to delete
		 * wrapWaitClickXpath(value2); wrapWaitClickXpath(value3);
		 * wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(value4)))
		 * ; // Thread.sleep(2000); } } } catch (Exception e) {
		 * System.out.println("Cannot find elements to Delete many"); } //
		 * Thread.sleep(1000); break;
		 */

		case "SENDFILE":
			// System.out.println(step);
			try {
				wrapWaitSendFileXpath(value1, key1);
			} catch (Exception e) {
				System.out.println("Cannot find element to Send Find");
			}
			// Thread.sleep(1000);
			break;

		case "SENDKEY_CURRENT_TIME_IN_FORMAT":
			// System.out.println(step);
			// driver.findElement(By.xpath(value1)).clear();
			// Thread.sleep(2000);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			Date currentTime = new Date(System.currentTimeMillis());
			// System.out.println(formatter.format(currentTime));
			wrapWaitSendKeysXpath(value1, formatter.format(currentTime));
			Thread.sleep(2000);
			break;

		case "SENDKEY_RANDOM":
			// System.out.println(step);
			try {
				wrapWaitSendKeysXpath(value1, key1 + rand + key2);
			} catch (Exception e) {
				System.out.println("Cannot find element to Send Keys");
			}
			// Thread.sleep(1000);
			break;

		case "SELECT_RANDOM_JS":
			// System.out.println(step);
			try {
				wrapWaitClickXpath(value1);
				wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(value2)));

				List<WebElement> allItemsElement = driver.findElements(By.xpath(value2));
				for (int i = 0; i < allItemsElement.size(); i++) {
					String itemText = allItemsElement.get(i).getText();
					// System.out.println("itemText: " +i +" = " + itemText);
					if (itemText.equals(key1)) {
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
								allItemsElement.get(i));
						allItemsElement.get(i).click();
						// Thread.sleep(3000);
						break;
					}
				}
			} catch (Exception e) {
				System.out.println("Cannot select Random JS");
			}
			break;

		case "SELECT_RANDOM":
			// System.out.println(step);
			try {
				wrapWaitClickXpath(value1);
				wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(value2)));
				List<WebElement> listings = driver.findElements(By.xpath(value2));
				Random r = new Random();
				int randomValue = r.nextInt(listings.size()); // Getting a random value that is between 0 and (list's
																// size)-1
				listings.get(randomValue).click(); // Clicking on the random item in the list.
			} catch (Exception e) {
				System.out.println("Cannot select Random");
			}
			break;

		case "SHADOW_ROOT":
			// System.out.println(step);

			WebElement root1 = driver.findElement(By.xpath("//input[@name='opentime']"));
			WebElement shadowRoot1 = (WebElement) ((JavascriptExecutor) driver)
					.executeScript("return arguments[0].shadowRoot", root1);

			// Get shadow root element
			// WebElement shadowRoot1 = expandRootElement(root1);
			String actualHeading = shadowRoot1.findElement(By.xpath("div[@id='placeholder']/div")).getText();
			System.out.println(actualHeading);
			break;

		case "GOTOURL":
			// System.out.println(step);
			// Get url of application
			// driver.get(p.getProperty(value));
			try {
				driver.get(value1);
			} catch (Exception e) {
				System.out.println("Cannot open page");
			}
			// Thread.sleep(1000);
			break;

		case "GETTEXT":
			// System.out.println(step);
			// Get text of an element
			// driver.findElement(this.getObject(p,objectName,objectType)).getText();
			// driver.findElement(this.getObject(objectName,objectType,value)).getText();
			try {
				wrapWaitGetTextXpath(value1);
			} catch (Exception e) {
				System.out.println("Cannot find element to Get Text");
			}
			// Thread.sleep(1000);
			break;

		case "SCROLLDOWN":
			// System.out.println(step);
			// scroll down to the bottom
			JavascriptExecutor js = (JavascriptExecutor) driver;
			try {
				js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Thread.sleep(1000);
			} catch (Exception e) {
				System.out.println("Cannot scroll to bottom 1st time");
			}
			try {
				js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Thread.sleep(1000);
			} catch (Exception e) {
				System.out.println("Cannot scroll to bottom 2nd time");
			}
			try {
				js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Thread.sleep(1000);
			} catch (Exception e) {
				System.out.println("Cannot scroll to bottom 3rd time");
			}
			try {
				js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Thread.sleep(1000);
			} catch (Exception e) {
				System.out.println("Cannot scroll to bottom 4th time");
			}
			try {
				js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				Thread.sleep(1000);
			} catch (Exception e) {
				System.out.println("Cannot scroll to bottom 5th time");
			}
			break;

		case "SLEEP":
			// System.out.println(step);
			Thread.sleep(1000);
			break;

		case "ASSERT":
			// System.out.println(step);
			try {
				String expectBefore = wrapWaitGetTextXpath(value1);
				// String expectBefore = wrapWaitGetTextXpath(value1).toLowerCase();
				// System.out.println(expectBefore);
				// String actualBefore =
				// driver.findElement(this.getObject(objectName,objectType,value)).getText();
				// String actualBefore =
				// driver.findElement(this.getObject(p,objectName,objectType)).getText();
				// String expect = StringUtils.removeAccent(expectBefore);
				// System.out.println("expectBefore: " + expectBefore);
				// System.out.println("assertValue: " +assertValue);
				Assert.assertTrue(expectBefore.contains(assertValue));
				// Assert.assertEquals(driver.findElement(this.getObject(p,objectName,objectType)).getText(),
				// value);
				System.out.println("Assert Ok");
			} catch (Exception e) {
				System.out.println("Assert Not Ok");
			}
			// Thread.sleep(1000);
			break;

		case "REQUIRED FIELD TEXT":
			List<WebElement> lst = driver.findElements(By.xpath(value1));
			/*
			 * for (int i = 0; i< lst.size(); i++) { }
			 */
			String actText = lst.get(1).getText();
			System.out.println("actText: " + actText);
			Assert.assertTrue(actText.contains(assertValue));
			break;

		case "ASSERT_TITLE":
			// System.out.println(step);
			Assert.assertEquals(driver.getTitle(), key1);
			break;

		case "DISPLAY":
			// System.out.println(step);
			try {
				driver.findElement(By.xpath(value1)).isDisplayed();
				System.out.println("Element is displayed.");
			} catch (Exception e) {
				System.out.println("Element is not displayed.");
			}
			break;

		case "LOGOUT":
			try {
				wrapWaitClickXpath(value1);
			} catch (Exception e) {
			}

		case "SPAN":
			try {
				wrapWaitClickXpath(value1);
			} catch (Exception e) {
			}

		default:
			break;
		}
	}

	/**
	 * Find element BY using object type and value
	 * 
	 * @param p
	 * @param objectName
	 * @param objectType
	 * @return
	 * @throws Exception
	 */
	// private By getObject(Properties p,String objectName,String objectType) throws
	// Exception{
	private By getObject(String objectName, String objectType, String value) throws Exception {
		// System.out.println("KeywordFW_UIOperation_ObjectType");
		// Find by id
		if (objectType.equalsIgnoreCase("ID")) {
			// return By.id(p.getProperty(objectName));
			return By.id(value);
		}
		// Find by xpath
		if (objectType.equalsIgnoreCase("XPATH")) {
			// return By.xpath(p.getProperty(objectName));
			return By.xpath(value);
		}
		// find by class
		else if (objectType.equalsIgnoreCase("CLASSNAME")) {
			// return By.className(p.getProperty(objectName));
			return By.className(value);
		}
		// find by name
		else if (objectType.equalsIgnoreCase("NAME")) {
			// return By.name(p.getProperty(objectName));
			return By.name(value);
		}
		// Find by css
		else if (objectType.equalsIgnoreCase("CSS")) {
			// return By.cssSelector(p.getProperty(objectName));
			return By.cssSelector(value);
		}
		// find by link
		else if (objectType.equalsIgnoreCase("LINK")) {
			// return By.linkText(p.getProperty(objectName));
			return By.linkText(value);
		}
		// find by partial link
		else if (objectType.equalsIgnoreCase("PARTIALLINK")) {
			// return By.partialLinkText(p.getProperty(objectName));
			return By.partialLinkText(value);
		} else {
			throw new Exception("Wrong object type");
		}
	}
}
