package priop_rutgon;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class rutgon {
	public WebDriver driver;
	public WebDriverWait wait;
	public JavascriptExecutor js;
	public WebElement element;
	// public FluentWait wait;

	//
	public rutgon(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver, 15);
		this.js = (JavascriptExecutor) driver;
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	// I. Define Xpath, Css, Id
	public WebElement wrapXpathVisible(String elementXpath) {
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath)));
	}

	public String wrapWaitGetTextXpath(String elementXpath) {
		element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath)));
		return
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))).getText();
		element.getText();
	}

	public void wrapWaitClickXpath(String elementXpath) {
		element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath)));
			js.executeScript("arguments[0].click();", element);
	}
	
	public void wrapWaitClickXpathJava(String elementXpath) {
		element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath)));
		element.click();
	}
	public void wrapWaitSendKeysXpath(String elementXpath, String keyXpath) {
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))).sendKeys(keyXpath);
		element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath)));
		element.sendKeys(keyXpath);
	}
	
	public void wrapWaitSendFileXpath(String elementXpath, String fileXpath) {
		//do element bi hidden nen fai dung presence
		WebElement addFiles = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));
		addFiles.sendKeys(fileXpath);
	}
	
	// Css
	public String wrapGetTextCss(String elementCss) {
		return driver.findElement(By.cssSelector(elementCss)).getText();
	}

	public String wrapWaitGetTextCss(String elementCss) {
		return wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(elementCss))).getText();
	}

	public void wrapClickCss(String elementCss) {
		driver.findElement(By.cssSelector(elementCss)).click();
	}

	public void wrapWaitClickCss(String elementCss) {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(elementCss))).click();
	}

	public void wrapSendKeysCss(String elementCss, String keyCss) {
		driver.findElement(By.cssSelector(elementCss)).sendKeys(keyCss);
	}

	public void wrapWaitSendKeysCss(String elementCss, String keyCss) {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(elementCss))).sendKeys(keyCss);
	}

	// Id
	public String wrapGetTextId(String elementId) {
		return driver.findElement(By.id(elementId)).getText();
	}

	public String wrapWaitGetTextId(String elementId) {
		return wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementId))).getText();
	}

	public void wrapClickId(String elementId) {
		driver.findElement(By.id(elementId)).click();
	}

	public void wrapWaitClickId(String elementId) {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementId))).click();
	}

	public void wrapSendKeysId(String elementId, String keyId) {
		driver.findElement(By.id(elementId)).sendKeys(keyId);
	}

	public void wrapWaitSendKeysId(String elementId, String keyId) {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementId))).sendKeys(keyId);
	}

	// Returns webelement
	public WebElement expandRootElement(WebElement element) {
		WebElement ele = (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].shadowRoot", element);
		return ele;

	}
}
