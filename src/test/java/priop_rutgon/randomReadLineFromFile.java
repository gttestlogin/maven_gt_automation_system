package priop_rutgon;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class randomReadLineFromFile {
	private String fileName = "C:\\Users\\LAP12778-local\\Downloads\\README.txt";
	List<String> lines = null;

	public static void main(String[] args) {
	    randomReadLineFromFile randomWord = new randomReadLineFromFile();
	    //System.out.println("Hello");
	    randomWord.init();
	}

	private void init() { //khoi tao ham doc
	    try {
	        lines = Files.readAllLines(Paths.get(fileName),
	                StandardCharsets.UTF_8); //read file from path
	    } catch (IOException e) {
	        System.out.println("File can't be opened.");//if cannot read, return message
	    }

	    int randomWordIndex = getRandomNumber(0, lines.size()-1); //get getRandomNumber method
	    System.out.println("lines.get(randomWordIndex): " +lines.get(randomWordIndex));
	}

	/**
	 * Returns a random number in the range of the specified min and max
	 * parameters.
	 * 
	 * @param min
	 *            the minimum value to return
	 * @param max
	 *            the maximum value to return
	 * @return a random number between the specified range
	 */
	private int getRandomNumber(int min, int max) {
	    return min + (int) (Math.random() * ((max - min) + 1));
	}
}
