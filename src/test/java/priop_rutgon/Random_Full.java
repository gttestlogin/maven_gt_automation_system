package priop_rutgon;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.testng.annotations.Test;

import java.util.Random;

public class Random_Full {
	private static String fileName = "C:\\Users\\manhtt\\Downloads\\README.txt";
	static List<String> lines = null;
	
	// a ThreadLocalRandom is initialized with an internally generated seed that may
	// not otherwise be modified. When
	// applicable, use of ThreadLocalRandom rather than shared Random objects in
	// concurrent programs will typically
	// encounter much less overhead and contention.
	private static void randomInInteger_getRandomInteger() {
		int crunchifyInteger = ThreadLocalRandom.current().nextInt(100, 150);
		System.out.println("RandomInteger: " + crunchifyInteger);

	}

	private static void randomInDouble_getRandomDouble() {
		double crunchifyDouble = ThreadLocalRandom.current().nextDouble(1, 250);
		System.out.println("RandomDouble: " + crunchifyDouble);

	}

	public static String randomInArray_getRandomCompany() {
		ArrayList<String> companyName = new ArrayList<String>();

		companyName.add("Google");
		companyName.add("Facebook");
		companyName.add("Twitter");
		companyName.add("Paypal");
		companyName.add("Uber");
		companyName.add("Yahoo");

		// Get Random Company Name from Arraylist using Random().nextInt()
		String company = companyName.get(new Random().nextInt(companyName.size()));
		return company;
	}

	/*
	private static void log(Object crunchifyObject) {
		System.out.println("crunchifyObject: " + crunchifyObject);
	}
	*/
	
	public static void randomInList() { 
		String[] str = { "New", "None", "Full", "Hot" };
		String rdStatusServer = str[new Random().nextInt(str.length)];
		System.out.println("rdStatusServer: " + rdStatusServer);
		
		String[] Tuket = {"VN", "HQ", "JP"};
		String DoiWin = Tuket[new Random().nextInt(Tuket.length)];
		System.out.println("DoiWin: " + DoiWin);
	}
	
	public static void randomFromAToZ() { //does not work
		int length1 = 0;
		Random randomw = new Random();
		final int alphabetLength = 'Z' - 'A' + 1;
		StringBuilder result = new StringBuilder(length1);
		while (result.length() < length1) {
			final char charCaseBit = (char) (randomw.nextInt(2) << 5);
			result.append((char) (charCaseBit | ('A' + randomw.nextInt(alphabetLength))));
		}
		System.out.println("result.toString(): " + result.toString());
	}
	
	public static void randomOneLetter() {
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String fullalphabet = alphabet + alphabet.toLowerCase() + "123456789";
		Random randomz = new Random();
		char code = fullalphabet.charAt(randomz.nextInt(9));
		System.out.println("Character.toString(code): " + Character.toString(code));

	}
	
	public static void randomOneLetter_2() {
		Random r = new Random();
		char way1 = (char) (r.nextInt(26) + 'a');
		char way2 = (char) (r.nextInt('z' - 'a') + 'a' + 1); // because 'z' - 'a' == 25, not 26 ki tu trong bang chu cai
		System.out.println("way1: " + way1);
		System.out.println("way2: " + way2);

		String alphabet = "123xyz";
		for (int i = 0; i < 2; i++) {// in ra 2 value
			System.out.println(
					"alphabet.charAt(r.nextInt(alphabet.length())): " + alphabet.charAt(r.nextInt(alphabet.length())));
		}

		String abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		char letter = abc.charAt(r.nextInt(abc.length()));
		System.out.println("letter: " + letter);
	}
	
	/*
	public static void randomLineFromFile() {
		Random_Full randomWord = new RandomWords();
		// randomWord.init();
		try {
			lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
		} catch (IOException e) {
			System.out.println("File can't be opened.");
		}

		int randomWordIndex = getRandomNumber(1, lines.size());// method getRandomNumber is below
		System.out.println("+ lines.get(randomWordIndex): " + lines.get(randomWordIndex));

		// return min + (int) (Math.random() * ((max - min) + 1));
	}

	private static int getRandomNumber(int min, int max) {// read the line of words from the file and return
		return min + (int) (Math.random() * ((max - min) + 1));
	}
	*/
	
	public static void randomInList_2() {
		String[] words = { "Jello", "Hello" };
		// Pick random index of words array (0 or 1)
		int randomWordNumber = (int) (Math.random() * words.length);
		// Create an array to store already entered letters
		char[] enteredLetters = new char[words[randomWordNumber].length()];
		System.out.println("+ randomWordNumber: " + randomWordNumber);
		System.out.println("+ enteredLetters: " + enteredLetters);
	}
	public static void randomWordWithLength() {
		final int PASSWORD_LENGTH = 8;
		StringBuffer sb1 = new StringBuffer();
		for (int x = 0; x < PASSWORD_LENGTH; x++) {
			sb1.append((char) ((int) (Math.random() * 26) + 97));// 97 ascii value, a-z (97 to 122) and A-Z (65 to 90)
																	// differs in 5th bit (2^5 or 1 << 5 or 32).
		}
		System.out.println("+ sb1.toString(): " + sb1.toString());
	}
	
	public static void stringBuilder() {
		// Tạo đối tượng StringBuilder
		// Hiện tại chưa có dữ liệu trên StringBuilder.
		StringBuilder sb = new StringBuilder(10);

		// Nối thêm chuỗi Hello vào sb.
		sb.append("Hello...");
		System.out.println("- sb after appends a string: " + sb);

		// append a character
		char c = '!';
		sb.append(c);
		System.out.println("- sb after appending a char: " + sb);

		// Trèn một String vào vị trí thứ 5
		sb.insert(5, " Java");
		System.out.println("- sb after insert string: " + sb);

		// Xóa đoạn String con trên StringBuilder.
		// Tại vị trí có chỉ số 5 tới 8
		sb.delete(5, 8);

		System.out.println("- sb after delete: " + sb);
	}
	
	
	public static void main(String[] args) {
	//public static void test() {
		for (int i = 1; i <= 10; i++) {

			System.out.println("Loop # " + i + " : " + randomInArray_getRandomCompany());
		}

		randomInDouble_getRandomDouble();
		randomInInteger_getRandomInteger();
		randomInList();
		//randomFromAToZ(); does not work now
		randomOneLetter();
		randomOneLetter_2();
		//randomLineFromFile(); 	chua xu li duoc thang nay
		//getRandomNumber(); 			chua xu li duoc thang nay
		randomInList_2();
		randomWordWithLength();
		stringBuilder();
		
	}
	
}