package priop_rutgon;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class randomPack_imcomplete extends rutgon{

	public randomPack_imcomplete(WebDriver driver) {
		super(driver);
	}
	
	@Test
	public void random() {
		List <WebElement> listings = driver.findElements(By.xpath("//*[contains(@class,'am-active')]//*[@class='lazy']"));
		for (int i=1; i < listings.size()-2; i++) { //ignore 1st pack and 2 last pack
			//System.out.println("Index " + i + ": " + listings.get(i).getAttribute("id") +listings.get(i).getAttribute("id"));
		}
		Random r = new Random();
		int randomValue = r.nextInt(listings.size()); //Getting a random value that is between 0 and (list's size)-1
		listings.get(randomValue).click(); //Clicking on the random item in the list.
		
		
		try {
			String a = listings.get(1).getAttribute("data-price");
			String b = listings.get(1).getAttribute("data-original");
			int c = Integer.parseInt(listings.get(1).getAttribute("data-price"));
			int d = Integer.parseInt(listings.get(1).getAttribute("data-original"));
			//String b = listings.get(randomValue).getAttribute("data-original").toString();
			System.out.println("a: " +a +" - " +c);
			System.out.println("b: " +b +" - " +d);
		}
		catch(Exception e) {}
		
	}
	
	public void test_randomPack() {
		this.random();
	}
}
