package priop_rutgon;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.testng.annotations.Test;

public class CurrentTimeDateCalendar {

	@Test
	public void getCurrentTimeUsingDate() {
		// TODO Auto-generated method stub
		
		long i = (long) (new Date().getTime());
	    System.out.println("Long : " + i);
	    String j = String.valueOf(i);
	    //j.substring(8);
	    System.out.println("i.substring(8) : " + j.substring(6));

		Date date = new Date();
		String addTime = "00:10"; //0 hour 10 minutes
		String strDateFormat = "YYYY-MM-dd HH:mm";
		DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
		String formattedDate = dateFormat.format(date);
		String formattedDateZZZ = dateFormat.format(date) + " " +addTime;
		System.out.println("Current time of the day using Date - 24 hour format: " + formattedDate);

		String strDateFormat1 = "YYYY-MM-dd hh:mm:ss a";
		DateFormat dateFormat1 = new SimpleDateFormat(strDateFormat1);
		String formattedDate1 = dateFormat1.format(date);
		System.out.println("Current time of the day using Date - 12 hour format: " + formattedDate1);

		System.out.println("-----Current time of your time zone-----");
		LocalTime time = LocalTime.now();
		System.out.println("Current time of the day: " + time);

		System.out.println("-----Current time of a different time zone using LocalTime-----");
		ZoneId zoneId = ZoneId.of("America/Los_Angeles");
		LocalTime localTime = LocalTime.now(zoneId);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
		String formattedTime = localTime.format(formatter);
		System.out.println("Current time of the day in Los Angeles: " + formattedTime);

		System.out.println("-----Current time of different offset-----");
		ZoneOffset zoneOffset = ZoneOffset.of("-08:00");
		ZoneId zoneId1 = ZoneId.ofOffset("UTC", zoneOffset);
		LocalTime offsetTime = LocalTime.now(zoneId1);
		DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("hh:mm a");
		DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("HH:mm:ss");
		String formattedTime1 = offsetTime.format(formatter1);
		String formattedTime2 = offsetTime.format(formatter2);
		System.out.println("Current time of the day with offset -08:00: " + formattedTime1);
		System.out.println("Current time of the day with offset -08:00: " + formattedTime2);
		
	}

	public void test_CurrentTimeDateCalendar() {
		this.getCurrentTimeUsingDate();
	}

}
