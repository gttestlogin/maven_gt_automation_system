package priop_rutgon;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SelectLoginMethod {
	
	public static WebDriver driver;
	public static WebDriverWait wait;
	
	public SelectLoginMethod (WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait (driver, 120);
		//this.wait = new FluentWait(driver).withTimeout(30, TimeUnit.SECONDS).pollingEvery(500, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class);
	}
	
	public static void main(String [] args) throws InterruptedException {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://zpay.mobi/musindo");
		Thread.sleep(1000);
		//If ZingID is default, then fill username/password
	
	try {
	
		if(driver.findElement(By.xpath("//*[@id='u']")).isDisplayed() || driver.findElement(By.xpath("//*[@class='form-signin-heading page-header']")).isDisplayed()) {
			System.out.println("1");
			driver.findElement(By.xpath("//*[@id='u']")).sendKeys("four_power1");
			driver.findElement(By.xpath("//*[@id='p']")).sendKeys("Han123do!");
		}
	}
	catch (Exception e) {}
		// If ZingId is not default, try to select ZingId as prio1 Login Method
	try {	
		if (driver.findElement(By.xpath("//*[@id='zing' or @id='other_zing' or @class='bt-zm']")).isDisplayed()) {
			System.out.println("2");
			driver.findElement(By.xpath("//*[@id='zing' or @id='other_zing' or @class='bt-zm']")).click();
			driver.findElement(By.xpath("//*[@id='u']")).sendKeys("four_power1");
			driver.findElement(By.xpath("//*[@id='p']")).sendKeys("Han123do!");
		}
	}
	catch (Exception e) {}
		//Try to select Email (Quick Login) as Login Method
	try {	
		if (driver.findElement(By.xpath("//*[@id='email' or @id='other_email']")).isDisplayed()) {
			System.out.println("3");
			driver.findElement(By.xpath("//*[@id='email' or @id='other_email']")).click();
		}
	}
	catch (Exception e) {}
		//Try to select Google Plus as Login Method
	try {	
		if (driver.findElement(By.xpath("//*[@id='google' or @id='other_google' or @class='action-ql google']")).isDisplayed()) {
			System.out.println("4");
			driver.findElement(By.xpath("//*[@id='google' or @id='other_google' or @class='action-ql google']")).click();
		}
	}
	catch (Exception e) {}
		//Try to select Facebook as Login Method
	try{
		if (driver.findElement(By.xpath("//*[@id='facebook' or @id='other_facebook' or @class='action-ql facebook']")).isDisplayed()) {
			System.out.println("5");
			driver.findElement(By.xpath("//*[@id='google' or @id='other_other_google' or @class='action-ql google']")).click();
		}
	}
	catch (Exception e) {}
		// Try to select Zalo as Login Method
	try {
		if (driver.findElement(By.xpath("//*[@id='zalo' or @id='other_zalo' or @class='action-ql zalo']")).isDisplayed()){
			System.out.println("6");
			driver.findElement(By.xpath("//*[@id='other_zalo' or @class='action-ql zalo']")).click();
		}
	}
	catch (Exception e) {}
	//}
	//catch (Exception e) {}
	}
}
