package priop_rutgon;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

//import jxl.write.DateTime;

public class dateTime {
	public static void main (String[] args) throws ParseException
	{
		// Create SimpleDateFormat object 
        SimpleDateFormat sdfo = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
  
        // Get the two dates to be compared 
        Date d1 = sdfo.parse("2019-05-06 11:35:09"); 
        Date d2 = sdfo.parse("2012-03-31 10:15:05"); 
  
        // Print the dates 
        System.out.println("Date1 : " + sdfo.format(d1)); 
        System.out.println("Date2 : " + sdfo.format(d2)); 
        
	}
}
