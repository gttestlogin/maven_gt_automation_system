package admin;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import priop_rutgon.CurrentTimeDateCalendar;

public class Admin_Full {
	public WebDriver driver;
	public WebDriverWait wait;
	CurrentTimeDateCalendar objCurrentTimeDateCalendar;
	// public FluentWait wait;

	/*
	 * public Group_Service(WebDriver driver) { this.driver = driver; this.wait =
	 * new WebDriverWait (driver, 120); //this.wait = new
	 * FluentWait(driver).withTimeout(30, TimeUnit.SECONDS).pollingEvery(500,
	 * TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class); }
	 */

	// I. Define Xpath, Css, Id
	public String wrapGetTextXpath(String elementXpath) {
		return driver.findElement(By.xpath(elementXpath)).getText();
	}

	public String wrapWaitGetTextXpath(String elementXpath) {
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))).getText();
	}

	public void wrapClickXpath(String elementXpath) {
		driver.findElement(By.xpath(elementXpath)).click();
	}

	public void wrapWaitClickXpath(String elementXpath) {
		// wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))).click();
	}

	public void wrapSendKeysXpath(String elementXpath, String keyXpath) {
		driver.findElement(By.xpath(elementXpath)).sendKeys(keyXpath);
	}

	public void wrapWaitSendKeysXpath(String elementXpath, String keyXpath) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))).sendKeys(keyXpath);
	}

	@BeforeTest
	public void setUp() {
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 120);
		driver.manage().window().maximize();
		driver.get("https://autosystem-dev.mto.zing.vn/#");
	}

	@Test(priority = 0)
	public void testLogin() throws InterruptedException {
		// Click on Agree button with User/Pass Login Method
		wrapWaitClickXpath("//*[@class='btn-submit']");
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='btn-submit']"))).click();

		// Fill username
		wrapWaitSendKeysXpath("//*[@name='loginfmt']", "manhtt@vng.com.vn");
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name='loginfmt']"))).sendKeys("manhtt@vng.com.vn");

		// Click on Next button
		wrapWaitClickXpath("//*[@id='idSIButton9']");
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='idSIButton9']"))).click();

		// Fill password
		wrapWaitSendKeysXpath("//*[@name='passwd']", "Han123do!");
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name='passwd']"))).sendKeys("Han12345do!");

		// Click on Sign in button
		wrapWaitClickXpath("//*[@id='idSIButton9']");
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='idSIButton9']"))).click();

		// Click on Don't ask again for 60 days
		// wrapWaitClickXpath("//*[@class='checkbox']");
		// Thread.sleep(2000);
		wrapWaitClickXpath("//*[@name='rememberMFA']");
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='checkbox']"))).click();

		// driver.findElement(By.xpath("//*[@name='otc']")).sendKeys("246442");
		// wrapWaitSendKeysXpath("//*[@name='otc']","246442");

		// Click on Verify button
		// Thread.sleep(2000);
		wrapWaitClickXpath("//*[@id='idSubmit_SAOTCC_Continue']");

		// Tick on Don't Show Again textbox
		// Thread.sleep(2000);
		wrapWaitClickXpath("//*[@name='DontShowAgain']");

		// Click on Yes button
		// Thread.sleep(2000);
		wrapWaitClickXpath("//*[@id='idSIButton9']");

		// create file named Cookies to store Login Information
		File file = new File("Cookies_AutoSystem.data");
		try {
			// Delete old file if exists
			file.delete();
			file.createNewFile();
			FileWriter fileWrite = new FileWriter(file);
			BufferedWriter Bwrite = new BufferedWriter(fileWrite);

			// Set<Cookie> someCookies = driver.manage().getCookies();
			// for (Cookie ck: someCookies) {

			// loop for getting the cookie information
			// Read the cookie information using: driver.manage().getCookies();
			// import import org.openqa.selenium.Cookie; here
			for (Cookie ck : driver.manage().getCookies()) {

				// Bwrite.write(("ck.getName(): " +ck.getName() + ";\n" + "ck.getValue(): "
				// +ck.getValue() + ";\n" + "ck.getDomain(): " +ck.getDomain() + ";\n" +
				// "ck.getPath(): " +ck.getPath() + ";\n"
				// + "ck.getExpiry(): " +ck.getExpiry() + ";\n" + "ck.isSecure(): "
				// +ck.isSecure()));
				Bwrite.write((ck.getName() + ";" + ck.getValue() + ";" + ck.getDomain() + ";" + ck.getPath() + ";"
						+ ck.getExpiry() + ";" + ck.isSecure()));
				Bwrite.newLine();
			}
			Bwrite.close();
			fileWrite.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// Verify user is brought to correct Page
		String expectedTitle1 = "GT System Operation Platform";
		String actualTitle1 = driver.getTitle();
		// System.out.println("^_^" + actualTitle1);
		Assert.assertTrue(expectedTitle1.contains(actualTitle1));
		Thread.sleep(1000);
	}

	@Test(priority = 1, enabled = false)
	public void test_New_Service_Maintenance() throws InterruptedException {
		try {
			// driver.navigate().refresh();
			// Click on New Service section
			wrapWaitClickXpath("//*[contains(text(),'New Service')]");

			// Check if the Page is New Service or not
			String expectedTitle2 = "New Service";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same New Service <-");
			} else
				System.out.println("-> Not New Service <-");
			/*
			 * try { String expectedTitle2 = "New Service"; String actualTitle2 =
			 * wrapWaitGetTextXpath("//*[@id='maintitle']");
			 * Assert.assertTrue(expectedTitle2.contains(actualTitle2)); } catch (Exception
			 * e) { //System.out.println("Not same name"); }
			 */

			// 1. Create New Service - Maintenance in Staging Environment
			// Click on New button
			wrapWaitClickXpath("//*[@class='new_data']");
			// Switch from main window to new pop-up
			// driver.switchTo().window(arg0)
			//// *[@id='mainform']
			// Product Name
			wrapWaitSendKeysXpath("//*[@name='productname']", "BPWPL");
			// Environment
			wrapWaitSendKeysXpath("//*[@name='environment']", "Staging");
			// Type Maintenance
			wrapWaitSendKeysXpath("//*[@name='addservicetype']", "Maintenance");
			// Service Name
			wrapWaitSendKeysXpath("//*[@name='servicename']", "A~!#@( ._-/)");
			// Script Name
			wrapWaitSendKeysXpath("//*[@name='servicescript']", "B~!#@( ._-/)");
			// Service Action
			wrapWaitSendKeysXpath("//*[@name='serviceaction']", "C~!#@( ._-/))");
			// Thread.sleep(10000);
			// Submit information
			wrapWaitClickXpath("//*[@type='submit']");
			Thread.sleep(1000);
			// Compare Notification Result Message
			String actualTitle3 = wrapWaitGetTextXpath("//*[@id='modal-title-modalnotify']").toLowerCase();
			if (actualTitle3.contains("success")) {
				System.out.println("-> Support all special characters/test_New_Service_Maintenance <-");
			} else if (actualTitle3.contains("exist")) {
				System.out.println(
						"-> Same field was existed already. Please search again/test_New_Service_Maintenance <-");
			} else
				System.out.println("-> Not support special characters/test_New_Service_Maintenance <-");

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 1) -> Failed");
			Thread.sleep(1000);
		}
	}

	@Test(priority = 2, enabled = false)
	public void test_New_Service_New_Server() throws InterruptedException {
		try {
			// driver.navigate().refresh();
			// Click on New Service section
			wrapWaitClickXpath("//*[contains(text(),'New Service')]");

			// Check if the Page is New Service or not
			String expectedTitle2 = "New Service";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same New Service <-");
			} else
				System.out.println("-> Not New Service <-");

			// 2. Create New Service - New Server in Staging Environment
			// Click on New button
			wrapWaitClickXpath("//*[@class='new_data']");
			// Product Name
			wrapWaitSendKeysXpath("//*[@name='productname']", "BPWPL");
			// Environment
			wrapWaitSendKeysXpath("//*[@name='environment']", "Staging");
			// Type Maintenance
			wrapWaitSendKeysXpath("//*[@name='addservicetype']", "New Server");
			// Service Name
			wrapWaitSendKeysXpath("//*[@name='servicename']", "D~!#@( ._-/)");
			// Script Name
			wrapWaitSendKeysXpath("//*[@name='servicescript']", "E~!#@( ._-/)");
			// Service Action
			wrapWaitSendKeysXpath("//*[@name='serviceaction']", "F~!#@( ._-/))");
			// Thread.sleep(10000);
			// Submit information
			wrapWaitClickXpath("//*[@type='submit']");
			Thread.sleep(1000);
			// Compare Notification Result Message
			String actualTitle4 = wrapWaitGetTextXpath("//*[@id='modal-title-modalnotify']").toLowerCase();
			if (actualTitle4.contains("success")) {
				System.out.println("-> Support all special characters/test_New_Service_New_Server <-");
			} else if (actualTitle4.contains("exist")) {
				System.out.println(
						"-> Same field was existed already. Please search again/test_New_Service_New_Server <-");
			} else
				System.out.println("-> Not support special characters/test_New_Service_New_Server <-");

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 2) -> Failed");
		}
	}

	@Test(priority = 3, enabled = false)
	public void test_New_Service_Up_patch() throws InterruptedException {
		try {
			// driver.navigate().refresh();
			// Click on New Service section
			wrapWaitClickXpath("//*[contains(text(),'New Service')]");

			// Check if the Page is New Service or not
			String expectedTitle2 = "New Service";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same New Service <-");
			} else
				System.out.println("-> Not New Service <-");

			// 3. Create New Service - Up-patch in Staging Environment
			// Click on New button
			wrapWaitClickXpath("//*[@class='new_data']");
			// Product Name
			wrapWaitSendKeysXpath("//*[@name='productname']", "BPWPL");
			// Environment
			wrapWaitSendKeysXpath("//*[@name='environment']", "Staging");
			// Type Maintenance
			wrapWaitSendKeysXpath("//*[@name='addservicetype']", "Up-patch");
			// Service Name
			wrapWaitSendKeysXpath("//*[@name='servicename']", "G~!#@( ._-/)");
			// Script Name
			wrapWaitSendKeysXpath("//*[@name='servicescript']", "H~!#@( ._-/)");
			// Service Action
			wrapWaitSendKeysXpath("//*[@name='serviceaction']", "I~!#@( ._-/))");
			// Thread.sleep(10000);
			// Submit information
			wrapWaitClickXpath("//*[@type='submit']");
			Thread.sleep(1000);
			// Compare Notification Result Message
			String actualTitle4 = wrapWaitGetTextXpath("//*[@id='modal-title-modalnotify']").toLowerCase();
			if (actualTitle4.contains("success")) {
				System.out.println("-> Support all special characters/test_New_Service_Up_patch <-");
			} else if (actualTitle4.contains("exist")) {
				System.out
						.println("-> Same field was existed already. Please search again/test_New_Service_Up_patch <-");
			} else
				System.out.println("-> Not support special characters/test_New_Service_Up_patch <-");

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 3) -> Failed");
		}
	}

	@Test(priority = 4, enabled = false)
	public void test_New_Service_Edit() throws InterruptedException {
		try {
			// driver.navigate().refresh();
			// Click on New Service section
			wrapWaitClickXpath("//*[contains(text(),'New Service')]");

			// Check if the Page is New Service or not
			String expectedTitle2 = "New Service";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same New Service <-");
			} else
				System.out.println("-> Not New Service <-");

			// Edit New Service in Staging Environment

			// Click on Edit button
			/*
			 * Does not work - dont know why List<WebElement> lst =
			 * driver.findElements(By.xpath("//*[@class='glyphicon glyphicon-edit']")); for
			 * (int i = 0; i < lst.size(); i++) { lst.get(0).click();
			 * System.out.println("Index " + i + ": " + lst.get(i).getText());
			 * Thread.sleep(5000); }
			 */
			try {
				wrapWaitClickXpath("//*[@class='glyphicon glyphicon-edit']");
			} catch (Exception e) {
				driver.findElements(By.xpath("//*[@class='glyphicon glyphicon-edit']")).get(0).click();
			}

			/*
			 * try { System.out.println("1"); // Sometimes this does not work
			 * wrapWaitClickXpath("//*[@class='glyphicon glyphicon-edit']");
			 * System.out.println("2"); Thread.sleep(2000); } catch (Exception e) {
			 * System.out.println("3");
			 * driver.findElements(By.xpath("//*[@class='glyphicon glyphicon-edit']")).get(0
			 * ).click(); System.out.println("4"); Thread.sleep(2000); }
			 */

			// Product Name
			// wrapWaitSendKeysXpath("//*[@name='productname']","BPWPL");

			// Environment
			// wrapWaitSendKeysXpath("//*[@name='environment']","Staging");

			// Type Maintenance
			// wrapWaitSendKeysXpath("//*[@name='addservicetype']","Maintenance");
			// Service Name
			try {
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@name='servicename']")).clear();
			} catch (Exception e) {
				System.out.println("Cannot edit Service Name");
			}
			wrapWaitSendKeysXpath("//*[@name='servicename']", "J~!#@( ._-/)");

			// Script Name
			try {
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@name='servicescript']")).clear();
			} catch (Exception e) {
				System.out.println("Cannot edit Script Name");
			}
			wrapWaitSendKeysXpath("//*[@name='servicescript']", "K~!#@( ._-/))");

			// Service Action
			try {
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@name='serviceaction']")).clear();
			} catch (Exception e) {
				System.out.println("Cannot edit Service Action");
			}
			wrapWaitSendKeysXpath("//*[@name='serviceaction']", "L~!#@( ._-/))");

			// Thread.sleep(10000);
			// Submit information
			wrapWaitClickXpath("//*[@type='submit']");
			Thread.sleep(1000);
			// Compare Notification Result Message
			String actualTitle3 = wrapWaitGetTextXpath("//*[@id='modal-title-modalnotify']").toLowerCase();
			if (actualTitle3.contains("success")) {
				System.out.println("-> Support all special characters/test_New_Service_Edit <-");
			} else if (actualTitle3.contains("exist")) {
				System.out.println("-> Same field was existed already. Please search again/test_New_Service_Edit <-");
			} else
				System.out.println("-> Not support special characters/test_New_Service_Edit <-");

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 4) test_New_Service_Edit -> Failed");
		}
	}

	@Test(priority = 5, enabled = false)
	public void test_New_Service_Delete() throws InterruptedException {
		try {
			// driver.navigate().refresh();
			// Click on New Service section
			wrapWaitClickXpath("//*[contains(text(),'New Service')]");

			// Check if the Page is New Service or not
			String expectedTitle2 = "New Service";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same New Service <-");
			} else
				System.out.println("-> Not New Service <-");

			// Remove New Service in Staging Environment

			// Click on Remove button
			/*
			 * Does not work - dont know why List<WebElement> lst =
			 * driver.findElements(By.xpath("//*[@class='glyphicon glyphicon-remove']"));
			 * for (int i = 0; i < lst.size(); i++) { lst.get(0).click();
			 * System.out.println("Index " + i + ": " + lst.get(i).getText());
			 * Thread.sleep(5000); }
			 */

			try {
				// Sometimes this does not work
				wrapWaitClickXpath("//*[@class='glyphicon glyphicon-remove']");
				// Thread.sleep(2000);
			} catch (Exception e) {
				driver.findElements(By.xpath("//*[@class='glyphicon glyphicon-remove']")).get(0).click();
				// Thread.sleep(2000);
			}

			// Confirm the DELETE action
			wrapWaitClickXpath("//*[contains(@class,'btn-primary')]");
			Thread.sleep(1000);
			// Compare Notification Result Message
			// try {
			// System.out.println("5");
			// String actualTitle6 =
			// wrapWaitGetTextXpath("//h4[contains(text(),'Success')]");
			String actualTitle6 = driver.findElement(By.xpath("//*[@class='bootbox-body']")).getText().toLowerCase();
			System.out.println(actualTitle6);
			if (actualTitle6.contains("success")) {
				System.out.println("-> Delete Successfully/test_New_Service_Delete <-");
			} else
				System.out.println("-> Do Not Delete Successfully/test_New_Service_Delete <-");
			// }
			// catch(Exception e) {System.out.println("cannot compare");}

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 5) test_New_Service_Delete -> Failed");
		}
	}

	@Test(priority = 6, enabled = false)
	public void test_Users_New() throws InterruptedException {
		try {
			// driver.navigate().refresh();
			// Click on Users section
			wrapWaitClickXpath("//*[contains(text(),'Users')]");

			// Check if the Page is New Service or not
			String expectedTitle2 = "Users";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same Users <-");
			} else
				System.out.println("-> Not Users <-");

			/*
			 * try { String expectedTitle2 = "New Service"; String actualTitle2 =
			 * wrapWaitGetTextXpath("//*[@id='maintitle']");
			 * Assert.assertTrue(expectedTitle2.contains(actualTitle2)); } catch (Exception
			 * e) { //System.out.println("Not same name"); }
			 */

			// 1. Create User in Staging Environment
			// Click on New button
			wrapWaitClickXpath("//*[@class='new_data']");

			// Product Name
			wrapWaitSendKeysXpath("//*[@name='productname']", "BPWPL");
			// User Name
			wrapWaitSendKeysXpath("//*[@name='username']", "nhutbm");
			// Environment
			wrapWaitSendKeysXpath("//*[@name='environment']", "Staging");
			// Select Role - Maintenance
			wrapWaitClickXpath("//*[@data-checkbox-index='0']");
			// Select Role - New Server
			wrapWaitClickXpath("//*[@data-checkbox-index='1']");
			// Select Role - Up-patch
			wrapWaitClickXpath("//*[@data-checkbox-index='2']");
			// Thread.sleep(10000);
			// Submit information
			wrapWaitClickXpath("//*[@type='submit']");

			// Compare Notification Result Message
			String actualTitle3 = wrapWaitGetTextXpath("//*[@id='modal-title-modalnotify']").toLowerCase();
			if (actualTitle3.contains("success")) {
				System.out.println("-> Support all special characters/test_Users_New <-");
			} else if (actualTitle3.contains("exist")) {
				System.out.println("-> Same field was existed already. Please search again/test_Users_New <-");
			} else
				System.out.println("-> Not support special characters/test_Users_New <-");

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 6) test_Users_New -> Failed");
		}
	}

	@Test(priority = 7, enabled = false)
	public void test_Users_Edit() throws InterruptedException {
		//try {
			// driver.navigate().refresh();
			// Click on Users section
			wrapWaitClickXpath("//*[contains(text(),'Users')]");

			// Check if the Page is New Service or not
			String expectedTitle2 = "Users";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same Users <-");	
			} else
				System.out.println("-> Not Users <-");

			// 2. Edit User in Staging Environment
			// Find user via Search function
			wrapWaitSendKeysXpath("//*[@type='search']", "nhutbm");

			// Click on Edit button
			try {
				wrapWaitClickXpath("//*[@class='glyphicon glyphicon-edit']");
			} catch (Exception e) {
				driver.findElements(By.xpath("//*[@class='glyphicon glyphicon-edit']")).get(0).click();
			}

			// Product Name
			// wrapWaitSendKeysXpath("//*[@name='productname']", "BPWPL");
			// User Name
			// wrapWaitSendKeysXpath("//*[@name='username']", "nhutbm");
			// Which the role do you want?
			wrapWaitSendKeysXpath("//*[@name='role']", "User");
			// Environment
			// wrapWaitSendKeysXpath("//*[@name='environment']", "Staging");

			// Remove any Role - New Server for example
			try {
				if (driver.findElement(By.xpath("//*[@data-checkbox-index='1']")).isSelected()) {
					wrapWaitClickXpath("//*[@data-checkbox-index='1']");
				} else
					System.out.println("Option is unselected already");
			} catch (Exception e) {
				System.out.println(e);
			}
			Thread.sleep(1000);
			// Thread.sleep(10000);
			// Submit information
			wrapWaitClickXpath("//*[@type='submit']");
			Thread.sleep(1000);
			// Compare Notification Result Message
			String actualTitle3 = wrapWaitGetTextXpath("//*[@id='modal-title-modalnotify']").toLowerCase();
			if (actualTitle3.contains("success")) {
				System.out.println("-> Support all special characters/test_Users_Edit <-");
			} else if (actualTitle3.contains("exist")) {
				System.out.println("-> Same field was existed already. Please search again/test_Users_Edit <-");
			} else
				System.out.println("-> Not support special characters/test_Users_Edit <-");

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		//} catch (Exception e) {
		//	System.out.println("@Test(priority = 7) test_Users_Edit -> Failed");
		//}
	}

	@Test(priority = 8, enabled = false)
	public void test_Users_Delete() throws InterruptedException {
		try {
			// driver.navigate().refresh();
			// Click on New Service section
			wrapWaitClickXpath("//*[contains(text(),'Users')]");

			// Check if the Page is New Service or not
			String expectedTitle2 = "Users";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same Users <-");
			} else
				System.out.println("-> Not Users <-");

			/*
			 * Find user via Search function - van chua xu li duoc Clear o day
			 * //driver.findElement(By.xpath("//*[@type='search']")).clear();
			 * //driver.findElement(By.xpath("//*[@type='search']")).clear();
			 * //wrapWaitSendKeysXpath("//*[@type='search']", "nhutbm"); WebElement
			 * searchText = driver.findElement(By.xpath("//*[@type='search']"));
			 * System.out.println("3"); String a = searchText.getText();
			 * System.out.println("a: "+a); Actions builder = new Actions(driver); Action
			 * seriesOfActions = builder .moveToElement(searchText) //.click()
			 * //.keyDown(searchText, Keys.SHIFT) //Text input will be in UPPERCASE
			 * //.sendKeys(searchText, "hello") //Type text "hello" //.keyUp(searchText,
			 * Keys.SHIFT) //This will highlight the text "hello" .doubleClick(searchText)
			 * //.contextClick() //This will bring up the context menu = right click
			 * //.keyUp(searchText, Keys.BACK_SPACE) .build(); seriesOfActions.perform(); //
			 * co dong nay thi cac chuoi action moi duoc thuc thi Thread.sleep(10000);
			 * System.out.println("4");
			 */

			// Click on Remove User button
			try {
				wrapWaitClickXpath("//*[@class='glyphicon glyphicon-remove']");
			} catch (Exception e) {
				driver.findElements(By.xpath("//*[@class='glyphicon glyphicon-remove']")).get(0).click();
			}

			// Confirm the DELETE action
			wrapWaitClickXpath("//*[contains(@class,'btn-primary')]");
			Thread.sleep(1000);
			// Compare Notification Result Message
			// try {
			// System.out.println("5");
			// String actualTitle6 =
			// wrapWaitGetTextXpath("//h4[contains(text(),'Success')]");
			String actualTitle6 = driver.findElement(By.xpath("//*[@class='bootbox-body']")).getText().toLowerCase();
			//System.out.println(actualTitle6);
			if (actualTitle6.contains("success")) {
				System.out.println("-> Delete Successfully/@Test(priority = 8) <-");
			} else
				System.out.println("-> Do Not Delete Successfully/@Test(priority = 8) <-");
			// }
			// catch(Exception e) {System.out.println("cannot compare");}

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 8) -> Failed");
		}
	}

	@Test(priority = 9, enabled = false)
	public void test_Group_Service_New() throws InterruptedException {
		try {
			// driver.navigate().refresh();
			// Click on Group Service section
			wrapWaitClickXpath("//*[contains(text(),'Group Service')]");

			// Check if the Page is New Service or not
			String expectedTitle2 = "Group Service";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same Group Service <-");
			} else
				System.out.println("-> Not Group Service <-");

			// Create Random
			int crunchifyInteger = ThreadLocalRandom.current().nextInt(2, 254);
			String _groupName = "Test" + crunchifyInteger;
			String _ipGroup = "{\"test\":\"10.10.157." + crunchifyInteger + "\"}";

			// 1. Create Group Service in Staging Environment
			// Click on New button
			wrapWaitClickXpath("//*[@class='new_data']");
			// Product Name
			wrapWaitSendKeysXpath("//*[@name='productname']", "BPWPL");
			// Group Name
			wrapWaitSendKeysXpath("//*[@name='groupname']", _groupName);
			// IP Group JSON
			wrapWaitSendKeysXpath("//*[@name='ipgroup']", _ipGroup);
			// Thread.sleep(10000);
			// Submit information
			wrapWaitClickXpath("//*[@type='submit']");

			// Compare Notification Result Message
			String actualTitle3 = wrapWaitGetTextXpath("//*[@id='modal-title-modalnotify']").toLowerCase();
			if (actualTitle3.contains("success")) {
				System.out.println("-> Data is inserted successfully/test_Group_Service_New <-");
			} else if (actualTitle3.contains("exist")) {
				System.out.println("-> Same Group Name was existed already. Please search again/test_Group_Service_New <-");
			} else
				System.out.println("-> Cannot create Data/test_Group_Service_New <-");

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 9) -> Failed");
		}
	}

	@Test(priority = 10, enabled = false) // chua xu li duoc phan xoa text
	public void test_Group_Service_Edit() throws InterruptedException, AWTException {
		try {
			//driver.navigate().refresh();
			Thread.sleep(2000);
			// Click on Group Service section
			wrapWaitClickXpath("//*[contains(text(),'Group Service')]");

			// Check if the Page is Group Service or not
			String expectedTitle2 = "Group Service";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same Group Service <-");
			} else
				System.out.println("-> Not Group Service <-");

			// Create Random
			int crunchifyInteger = ThreadLocalRandom.current().nextInt(2, 254);
			String _groupName = "Test" + crunchifyInteger;
			String _ipGroup = "{\"test\":\"10.10.157." + crunchifyInteger + "\"}";

			// 2. Edit User in Staging Environment
			// Find Test via Group Service section
			wrapWaitSendKeysXpath("//*[@type='search']", "Test");
			// Click on Edit button
			try {
				wrapWaitClickXpath("//*[@class='glyphicon glyphicon-edit']");
			} catch (Exception e) {
				driver.findElements(By.xpath("//*[@class='glyphicon glyphicon-edit']")).get(0).click();
			}

			// Edit Product Name
			// wrapWaitSendKeysXpath("//*[@name='productname']", "BPWPL");
			// Edit Group Name
			// wrapWaitSendKeysXpath("//*[@name='groupname']", _groupName);
			// Edit IP Group Json
			try {
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@name='ipgroup']")).clear();
			} catch (Exception e) {
				System.out.println("Cannot edit IP Group Json");
			}
			wrapWaitSendKeysXpath("//*[@name='ipgroup']", _ipGroup);
			// Thread.sleep(1000);
			// Submit information
			wrapWaitClickXpath("//*[@type='submit']");
			wrapWaitClickXpath("//*[@type='submit']");
			Thread.sleep(1000);
			// Compare Notification Result Message
			try {

				if (driver.findElement(By.xpath("//*[@id='modal-body-modalnotify']")).isDisplayed()) {
					String actualTitle6 = driver.findElement(By.xpath("//*[@id='modal-body-modalnotify']")).getText()
							.toLowerCase();
					if (actualTitle6.contains("success")) {
						System.out.println("-> Data is updated successfully/test_Group_Service_Edit <-");
					} else if (actualTitle6.contains("exist")) {
						System.out
								.println("-> Data was existed already. Please search again/test_Group_Service_Edit <-");
					} else
						System.out.println("Snt wrong with modal-body-modalnotify/test_Group_Service_Edit");
				}

			} catch (Exception e) {
				if (driver.findElement(By.xpath("//*[@class='help-block alpaca-message alpaca-message-notOptional']"))
						.isDisplayed()) {
					String errorMess = driver
							.findElement(By.xpath("//*[@class='help-block alpaca-message alpaca-message-notOptional']"))
							.getText().toLowerCase();
					if (errorMess.contains("invalid/test_Group_Service_Edit")) {
						System.out.println("-> Invalid Data/test_Group_Service_Edit <-");
					} else
						System.out.println("Smt wrong with Invalid/test_Group_Service_Edit");
				}
			}
			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 10) -> Failed");
		}
	}

	@Test(priority = 11, enabled = false)
	public void test_Group_Service_Delete() throws InterruptedException {
		try {
			// driver.navigate().refresh();
			// Click on Group Service section
			wrapWaitClickXpath("//*[contains(text(),'Group Service')]");

			// Check if the Page is Group Service or not
			String expectedTitle2 = "Group Service";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same Group Service <-");
			} else
				System.out.println("-> Not Group Service <-");

			// Find Test via Group Service function
			// wrapWaitSendKeysXpath("//*[@type='search']", "Test");

			// Click on Remove Group Service button
			try {
				wrapWaitClickXpath("//*[@class='glyphicon glyphicon-remove']");
			} catch (Exception e) {
				driver.findElements(By.xpath("//*[@class='glyphicon glyphicon-remove']")).get(0).click();
			}

			// Confirm the DELETE action
			wrapWaitClickXpath("//*[contains(@class,'btn-primary')]");
			Thread.sleep(1000);

			// Compare Notification Result Message
			String actualTitle6 = driver.findElement(By.xpath("//*[@class='bootbox-body']")).getText().toLowerCase();
			if (actualTitle6.contains("success")) {
				System.out.println("-> Data is deleted successfully/test_Group_Service_Delete <-");
			} else
				System.out.println("Cannot Delete Data/test_Group_Service_Delete");

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 11) -> Failed");
		}
	}

	@Test(priority = 12, enabled = false)
	public void HistorySection() throws InterruptedException {
		try {
			// driver.navigate().refresh();
			// Click on History section
			wrapWaitClickXpath("//a[contains(text(),'History')]");
			// Check if the Page is Group Service or not
			String expectedTitle2 = "History";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same History <-");
			} else
				System.out.println("-> Not History <-");
			
			Thread.sleep(2000);//Trong qua trinh chuyen trang, History dang load, nhung da co the check the Show Function, do do de an toan ta cho 1 Sleep
			// Check the Show function
			wrapWaitSendKeysXpath("//*[@name='mainshow_length']", "25");
			String mainshow_info = wrapWaitGetTextXpath("//*[@id='mainshow_info']");
			System.out.println("mainshow_info: " + mainshow_info);
			// Thread.sleep(5000);
			if (mainshow_info.contains("25")) {
				System.out.println("Show Function works correctly");
			} else
				System.out.println("Show Function works incorrectly");

			// Check the Search Function
			wrapWaitSendKeysXpath("//*[@type='search']", "nhutbm");
			Thread.sleep(1000);
			String user = driver.findElements(By.xpath("//*[@class='odd']")).get(0).getText();
			System.out.println("user: " + user);
			// Thread.sleep(5000);
			if (user.contains("nhutbm")) {
				System.out.println("Search Function works correctly/HistorySection");
			} else
				System.out.println("Search Function works incorrectly/HistorySection");
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 12) -> Failed");
		}
	}

	@Test(priority = 13, enabled = false)
	public void test_New_Server() throws InterruptedException {
		try {
			// driver.navigate().refresh();
			// Click on New Service section
			wrapWaitClickXpath("//*[contains(text(),'New Server')]");

			// Check if the Page is New Server or not
			String expectedTitle2 = "New Server";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same New Server <-");
			} else
				System.out.println("-> Not New Server <-");

			// Create Random Server ID
			int crunchifyInteger = ThreadLocalRandom.current().nextInt(-255, 254000);
			String _serverid = String.format("%d", crunchifyInteger);
			

			
			/*
			 * way 2 to convert from Integer to String Integer intInstance = new
			 * Integer(crunchifyInteger); String _serverid = intInstance.toString();
			 */

			// Get current time
			Date date = new Date();
			String strDateFormat = "YYYY-MM-dd HH:mm";
			DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
			String currentTime = dateFormat.format(date);

			// Create Random Word
			final int PASSWORD_LENGTH = 8; // do dai word = 8
			StringBuffer sb = new StringBuffer();// StringBuilder va StringBuffer la giong nhau, co the thay doi, xu ly
													// van
													// ban nhieu luong thi nen dung StringBuffer
			for (int x = 0; x < PASSWORD_LENGTH; x++) {
				sb.append((char) ((int) (Math.random() * 26) + 97));// 97 ascii value, a-z (97 to 122) and A-Z (65 to
																	// 90)
																	// differs in 5th bit (2^5 or 1 << 5 or 32).
			}
			String randomUrl = sb.toString();

			// Create Random among Status Server
			String[] str = { "New", "None", "Full", "Hot" };
			String rdStatusServer = str[new Random().nextInt(str.length)];

			// objRandomWords = new RandomWords();
			// objRandomWords.test_RandomWord();
			// 3. Create New Service - Up-patch in Staging Environment
			// Click on New button
			wrapWaitClickXpath("//*[@class='new_data']");
			// Product Name
			wrapWaitSendKeysXpath("//*[@name='productname']", "BPWPL");
			// Environment
			wrapWaitSendKeysXpath("//*[@name='environment']", "Staging");
			// Type Server ID
			wrapWaitSendKeysXpath("//*[@name='serveridadd']", _serverid);
			//Do Server ID hien len bang goi y tu, nen can bam vo khoang trang cua Environment de skip goi y
			wrapWaitClickXpath("//*[@id='modal-title']");
			// Server Name
			wrapWaitSendKeysXpath("//*[@name='servernameadd']", randomUrl);
			// Url
			wrapWaitSendKeysXpath("//*[@name='url']", "http://" + randomUrl + ".com");
			// Status Server
			wrapWaitSendKeysXpath("//*[@name='statusserver']", rdStatusServer);
			// Choose the open time
			wrapWaitSendKeysXpath("//*[@name='opentime']", currentTime);

			// *[@class='bootstrap-datetimepicker-widget']

			// Submit information
			wrapWaitClickXpath("//*[@type='submit']");
			Thread.sleep(1000);
			// Compare Notification Result Message
			String actualTitle4 = wrapWaitGetTextXpath("//*[@id='modal-title-modalnotify']").toLowerCase();
			if (actualTitle4.contains("success")) {
				System.out.println("-> Data is inserted successfully/test_New_Server <-");
			} else if (actualTitle4.contains("exist")) {
				System.out.println("-> Please search and edit if you want/test_New_Server <-");
			} else
				System.out.println("-> Something wrong with modalnotify/test_New_Server <-");

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 13) -> Failed");
		}
	}

	@Test(priority = 14, enabled = false)
	public void test_Up_Patch_New_patchfile() throws InterruptedException, AWTException {
		try {
			// driver.navigate().refresh();
			// Click on Up-patch section
			wrapWaitClickXpath("//*[contains(text(),'Up-patch')]");

			// Check if the Page is New Server or not
			String expectedTitle2 = "Up-patch";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same Up_Patch <-");
			} else
				System.out.println("-> Not Up_Patch <-");

			// Create Up-patch in Staging Environment
			// Click on New button
			wrapWaitClickXpath("//*[@class='new_data']");
			// Product Name
			wrapWaitSendKeysXpath("//*[@name='productname']", "BPWPL");

			// Environment
			wrapWaitSendKeysXpath("//*[@name='environment']", "Staging");

			// Server ID List
			// wrapWaitClickXpath("//*[@title='None']");
			wrapWaitClickXpath("//*[@class='multiselect dropdown-toggle btn btn-default']");
			try {
				Thread.sleep(1000);
				driver.findElements(By.xpath("//*[@class='checkbox']")).get(0).click();
				//System.out.println("Server ID content: "+ driver.findElements(By.xpath("//*[@class='checkbox']")).get(0).getText());
			} catch (Exception e) {
				System.out.println("Nothing here");
			}
			// Click again to exit selection
			wrapWaitClickXpath("//*[@class='multiselect dropdown-toggle btn btn-default']");

			// Which the patch type do you want? patchfile
			wrapWaitSendKeysXpath("//*[@name='type']", "patchfile");
			// upload Patch File
			// tab and enter Choose File
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyPress(KeyEvent.VK_ENTER);
			/*
			 * Click on Choose File button
			 * wrapWaitClickXpath("//input[@name='fileToUpload']"); System.out.println("4");
			 * wrapWaitClickXpath("//*[@class='fileupload-add-button']");
			 */

			// Perform series of actions to upload README file
			Thread.sleep(1000);
			try {
				StringSelection ss = new StringSelection("C:\\Users\\manhtt\\Downloads\\README.txt");
				// C:\Users\manhtt\Downloads\README.txt
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
				robot.keyPress(KeyEvent.VK_CONTROL);
				Thread.sleep(1000);
				robot.keyPress(KeyEvent.VK_V);
				Thread.sleep(1000);
				robot.keyRelease(KeyEvent.VK_V);
				Thread.sleep(1000);
				robot.keyRelease(KeyEvent.VK_CONTROL);
				Thread.sleep(1000);
				robot.keyPress(KeyEvent.VK_ENTER);
				Thread.sleep(1000);
				robot.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(1000);
			} catch (Exception e) {
				System.out.println("No way");
			}
			Thread.sleep(1000);
			// Submit information
			wrapWaitClickXpath("//*[@type='submit']");
			Thread.sleep(1000);
			// Compare Notification Result Message
			String actualTitle3 = wrapWaitGetTextXpath("//*[@id='modal-title-modalnotify']").toLowerCase();
			if (actualTitle3.contains("success")) {
				System.out.println("-> Data is inserted successfully/test_Up_Patch_New_patchfile <-");
			} else if (actualTitle3.contains("exist")) {
				System.out.println(
						"-> Same field was existed already. Please search again/test_Up_Patch_New_patchfile <-");
			} else
				System.out.println("-> Not support 14/test_Up_Patch_New_patchfile <-");

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 14) -> Failed");
		}
	}

	@Test(priority = 15, enabled = false)
	public void test_Up_Patch_New_patchname() throws InterruptedException, AWTException {
		try {
			// driver.navigate().refresh();
			// Click on Up-patch section
			wrapWaitClickXpath("//*[contains(text(),'Up-patch')]");

			// Check if the Page is New Server or not
			String expectedTitle2 = "Up-patch";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same Up_Patch <-");
			} else
				System.out.println("-> Not Up_Patch <-");

			// Create Random Word
			final int PASSWORD_LENGTH = 8; // do dai word = 8
			StringBuffer sb = new StringBuffer();// StringBuilder va StringBuffer la giong nhau, co the thay doi, xu ly
													// van
													// ban nhieu luong thi nen dung StringBuffer
			for (int x = 0; x < PASSWORD_LENGTH; x++) {
				sb.append((char) ((int) (Math.random() * 26) + 97));// 97 ascii value, a-z (97 to 122) and A-Z (65 to
																	// 90)
																	// differs in 5th bit (2^5 or 1 << 5 or 32).
			}
			String randomUrl = sb.toString();

			// Create Up-patch in Staging Environment
			// Click on New button
			wrapWaitClickXpath("//*[@class='new_data']");
			// Product Name
			wrapWaitSendKeysXpath("//*[@name='productname']", "BPWPL");
			// Environment
			wrapWaitSendKeysXpath("//*[@name='environment']", "Staging");
			// Server ID List
			// wrapWaitClickXpath("//*[@title='None']");
			wrapWaitClickXpath("//*[@class='multiselect dropdown-toggle btn btn-default']");
			try {
				driver.findElements(By.xpath("//*[@class='checkbox']")).get(0).click();
				//System.out.println("Server ID content: "+ driver.findElements(By.xpath("//*[@class='checkbox']")).get(0).getText());
			} catch (Exception e) {
				System.out.println("Nothing here");
			}
			// Click again to exit selection
			wrapWaitClickXpath("//*[@class='multiselect dropdown-toggle btn btn-default']");
			// Which the patch type do you want? patchname
			wrapWaitSendKeysXpath("//*[@name='type']", "patchname");
			// Fill name
			wrapWaitSendKeysXpath("//*[@name='patchname']", randomUrl);

			// Submit information
			wrapWaitClickXpath("//*[@type='submit']");
			Thread.sleep(1000);
			// Compare Notification Result Message
			String actualTitle3 = wrapWaitGetTextXpath("//*[@id='modal-title-modalnotify']").toLowerCase();
			if (actualTitle3.contains("success")) {
				System.out.println("-> Data is inserted successfully/test_Up_Patch_New_patchname <-");
			} else if (actualTitle3.contains("exist")) {
				System.out.println(
						"-> Same field was existed already. Please search again/test_Up_Patch_New_patchname <-");
			} else
				System.out.println("-> Not support 15/test_Up_Patch_New_patchname <-");

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 15) -> Failed");
		}
	}

	@Test(priority = 16, enabled = false)
	public void test_Maintenance_New() throws InterruptedException, AWTException {
		try {
			// driver.navigate().refresh();
			// Click on Maintenance section
			wrapWaitClickXpath("//*[contains(text(),'Maintenance')]");

			// Check if the Page is Maintenance or not
			String expectedTitle2 = "Maintenance";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same Maintenance <-");
			} else
				System.out.println("-> Not Maintenance <-");

			// Create Random Word
			final int PASSWORD_LENGTH = 8; // do dai word = 8
			StringBuffer sb = new StringBuffer();// StringBuilder va StringBuffer la giong nhau, co the thay doi, xu ly
													// van
													// ban nhieu luong thi nen dung StringBuffer
			for (int x = 0; x < PASSWORD_LENGTH; x++) {
				sb.append((char) ((int) (Math.random() * 26) + 97));// 97 ascii value, a-z (97 to 122) and A-Z (65 to
																	// 90)
																	// differs in 5th bit (2^5 or 1 << 5 or 32).
			}
			String randomUrl = sb.toString();

			Thread.sleep(1000);
			// Create Maintenance in Staging Environment
			// Click on New button
			wrapWaitClickXpath("//*[@class='new_data']");
			// Product Name
			wrapWaitSendKeysXpath("//*[@name='productname']", "BPWPL");

			// Environment
			wrapWaitSendKeysXpath("//*[@name='environment']", "Staging");

			// Server List
			wrapWaitClickXpath("//*[contains(@class,'multiselect dropdown-toggle')]");
			try {
				Thread.sleep(1000);
				List<WebElement> lst4 = driver.findElements(By.xpath("//*[@class='checkbox']"));
				// Getting size of options available
				int size = lst4.size();
				// Generate a random number with in range
				int randnMumber = ThreadLocalRandom.current().nextInt(0, size);
				// Selecting random value
				lst4.get(randnMumber).click();
				// driver.findElements(By.xpath("//*[@class='checkbox']")).get(0).click();
				//System.out.println("Server List: " + lst4.get(randnMumber).getText());
			} catch (Exception e) {
				System.out.println("No Server List");
			}
			Thread.sleep(5000);
			// Click again to exit selection
			wrapWaitClickXpath("//*[@class='multiselect dropdown-toggle btn btn-default']");
			// Service Name
			try {
				Thread.sleep(1000);
				List<WebElement> lst5 = driver.findElements(By.xpath("//*[@name='servicename1']/child::option"));
				// Getting size of options available
				int size5 = lst5.size();
				// Generate a random number with in range
				int randnMumber5 = ThreadLocalRandom.current().nextInt(0, size5);
				// Selecting random value
				lst5.get(randnMumber5).click();
				// driver.findElements(By.xpath("//*[@class='checkbox']")).get(0).click();
				//System.out.println("Server Name List: " + lst5.get(randnMumber5).getText());
			} catch (Exception e) {
				System.out.println("No Service Name1");
			}

			// Action
			try {
				Thread.sleep(1000);
				List<WebElement> lst6 = driver.findElements(By.xpath("//*[@name='serviceaction1']/child::option"));
				// Getting size of options available
				int size6 = lst6.size();
				// Generate a random number with in range
				int randnMumber6 = ThreadLocalRandom.current().nextInt(0, size6);
				// Selecting random value
				lst6.get(randnMumber6).click();
				// driver.findElements(By.xpath("//*[@class='checkbox']")).get(0).click();
				//System.out.println("Action List: " + lst6.get(randnMumber6).getText());
			} catch (Exception e) {
				System.out.println("No Service Action1");
			}
			Thread.sleep(1000);

			// Params
			wrapWaitClickXpath("//*[@data-checkbox-value='param1_enable']");

			// Params Input
			wrapWaitSendKeysXpath("//*[@name='params_input1']", randomUrl);// Not support special character !@#$( ._-/)

			// Add more
			wrapWaitClickXpath("//*[@data-key='add']");

			// Service Name
			try {
				Thread.sleep(1000);
				List<WebElement> lst7 = driver.findElements(By.xpath("//*[@name='servicename2']/child::option"));
				// Getting size of options available
				int size7 = lst7.size();
				// Generate a random number with in range
				int randnMumber7 = ThreadLocalRandom.current().nextInt(0, size7);
				// Selecting random value
				lst7.get(randnMumber7).click();
				// driver.findElements(By.xpath("//*[@class='checkbox']")).get(0).click();
				//System.out.println("Server Name2 List: " + lst7.get(randnMumber7).getText());
			} catch (Exception e) {
				System.out.println("No Service Name2");
			}

			// Action
			try {
				Thread.sleep(1000);
				List<WebElement> lst8 = driver.findElements(By.xpath("//*[@name='serviceaction2']/child::option"));
				// Getting size of options available
				int size8 = lst8.size();
				// Generate a random number with in range
				int randnMumber8 = ThreadLocalRandom.current().nextInt(0, size8);
				// Selecting random value
				lst8.get(randnMumber8).click();
				// driver.findElements(By.xpath("//*[@class='checkbox']")).get(0).click();
				//System.out.println("Action2 List: " + lst8.get(randnMumber8).getText());
			} catch (Exception e) {
				System.out.println("No Service Action2");
			}
			Thread.sleep(1000);

			// Submit information
			wrapWaitClickXpath("//*[@type='submit']");
			Thread.sleep(1000);
			// Compare Notification Result Message
			String actualTitle3 = wrapWaitGetTextXpath("//*[@id='modal-title-modalnotify']").toLowerCase();
			if (actualTitle3.contains("success")) {
				System.out.println("-> Data is inserted successfully <-");
			} else if (actualTitle3.contains("exist")) {
				System.out.println("-> Same field was existed already. Please search again <-");
			} else if (driver.findElements(By.xpath("//*[@class='glyphicon glyphicon-exclamation-sign']")).get(0)
					.getText().toLowerCase().contains("duplicate")) {
				System.out.println("-> Data Duplicate, Please Check !!! <-");
			} else
				System.out.println("-> Not support 16 <-");

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("@Test(priority = 16) -> Failed");
		}
	}

	@Test(priority = 17, enabled = false)
	public void test_Deployment_New_Ansible() throws InterruptedException, AWTException {
		//try {
			// driver.navigate().refresh();
			// Click on Maintenance section
			wrapWaitClickXpath("//*[contains(text(),'Deployment')]");

			// Check if the Page is Maintenance or not
			String expectedTitle2 = "Deployment";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same Deployment <-");
			} else
				System.out.println("-> Not Deployment <-");

			// Create Random Word
			final int PASSWORD_LENGTH = 8; // do dai word = 8
			StringBuffer sb = new StringBuffer();// StringBuilder va StringBuffer la giong nhau, co the thay doi, xu ly
													// van
													// ban nhieu luong thi nen dung StringBuffer
			for (int x = 0; x < PASSWORD_LENGTH; x++) {
				sb.append((char) ((int) (Math.random() * 26) + 97));// 97 ascii value, a-z (97 to 122) and A-Z (65 to
																	// 90)
																	// differs in 5th bit (2^5 or 1 << 5 or 32).
			}
			String randomUrl = sb.toString();

			// Create Up-patch in Staging Environment
			// Click on New button
			wrapWaitClickXpath("//*[@class='new_data']");
			// Product Name
			wrapWaitSendKeysXpath("//*[@name='productname']", "BPWPL");
			// IP Type
			wrapWaitSendKeysXpath("//*[@name='iptype']", "groupaddlist");
			
			// Group List select random
			try {
				Thread.sleep(1000);
				List<WebElement> lst9 = driver.findElements(By.xpath("//*[@name='groupaddlist']/child::option"));
				// Getting size of options available
				int size9 = lst9.size();
				// Generate a random number with in range
				int randnMumber9 = ThreadLocalRandom.current().nextInt(1, size9);//skip 0 = None Group List
				// Selecting random value
				lst9.get(randnMumber9).click();
				// driver.findElements(By.xpath("//*[@class='checkbox']")).get(0).click();
				System.out.println("Group List: " + lst9.get(randnMumber9).getText());
			} catch (Exception e) {
				System.out.println("No Group List");
			}
			
			Thread.sleep(1000);
			//Open Group IP List
			wrapWaitClickXpath("//*[contains(@class,'multiselect dropdown-toggle')]");
			//Select random Group IP List
			try {
				Thread.sleep(1000);
				List<WebElement> lst10 = driver.findElements(By.xpath("//*[@type='checkbox']"));
				// Getting size of options available
				int size10 = lst10.size();
				// Generate a random number with in range
				int randnMumber10 = ThreadLocalRandom.current().nextInt(0, size10);//skip 0 = None Group List
				// Selecting random value
				lst10.get(randnMumber10).click();
				// driver.findElements(By.xpath("//*[@class='checkbox']")).get(0).click();
				System.out.println("Group List: " + lst10.get(randnMumber10).getText());
			} catch (Exception e) {
				System.out.println("No Group List");
			}
			//Select again to close selection
			wrapWaitClickXpath("//*[contains(@class,'multiselect dropdown-toggle')]");
			
			//Case 1: Install Type = Ansible
			wrapWaitClickXpath("//*[@value='ansible']");
			wrapWaitClickXpath("//*[@value='ls1.yml']");

			// Submit information
			wrapWaitClickXpath("//*[@type='submit']");
			Thread.sleep(1000);
			// Compare Notification Result Message
			String actualTitle3 = wrapWaitGetTextXpath("//*[@id='modal-title-modalnotify']").toLowerCase();
			if (actualTitle3.contains("success")) {
				System.out.println("-> Data is inserted successfully/test_Deployment_New <-");
			} else if (actualTitle3.contains("exist")) {
				System.out.println("-> Same field was existed already. Please search again/test_Deployment_New <-");
			} else
				System.out.println("-> Not support 17/test_Deployment_New <-");

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		//} catch (Exception e) {
		//	System.out.println("@Test(priority = 17) -> Failed");
		//}
	}

	@Test(priority = 18, enabled = false)
	public void test_Deployment_New_Yum() throws InterruptedException, AWTException {
		//try {
			// driver.navigate().refresh();
			// Click on Maintenance section
			wrapWaitClickXpath("//*[contains(text(),'Deployment')]");

			// Check if the Page is Maintenance or not
			String expectedTitle2 = "Deployment";
			String actualTitle2 = wrapWaitGetTextXpath("//*[@id='maintitle']");
			if (actualTitle2.contains(expectedTitle2)) {
				System.out.println("-> Same Deployment <-");
			} else
				System.out.println("-> Not Deployment <-");

			// Create Random Word
			final int PASSWORD_LENGTH = 8; // do dai word = 8
			StringBuffer sb = new StringBuffer();// StringBuilder va StringBuffer la giong nhau, co the thay doi, xu ly
													// van
													// ban nhieu luong thi nen dung StringBuffer
			for (int x = 0; x < PASSWORD_LENGTH; x++) {
				sb.append((char) ((int) (Math.random() * 26) + 97));// 97 ascii value, a-z (97 to 122) and A-Z (65 to
																	// 90)
																	// differs in 5th bit (2^5 or 1 << 5 or 32).
			}
			String randomUrl = sb.toString();

			// Create Up-patch in Staging Environment
			// Click on New button
			wrapWaitClickXpath("//*[@class='new_data']");
			// Product Name
			wrapWaitSendKeysXpath("//*[@name='productname']", "BPWPL");
			// IP Type
			wrapWaitSendKeysXpath("//*[@name='iptype']", "groupaddlist");
			
			// Group List select random
			try {
				Thread.sleep(1000);
				List<WebElement> lst9 = driver.findElements(By.xpath("//*[@name='groupaddlist']/child::option"));
				// Getting size of options available
				int size9 = lst9.size();
				// Generate a random number with in range
				int randnMumber9 = ThreadLocalRandom.current().nextInt(1, size9);//skip 0 = None Group List
				// Selecting random value
				lst9.get(randnMumber9).click();
				// driver.findElements(By.xpath("//*[@class='checkbox']")).get(0).click();
				System.out.println("Group List: " + lst9.get(randnMumber9).getText());
			} catch (Exception e) {
				System.out.println("No Group List");
			}
			
			Thread.sleep(1000);
			//Open Group IP List
			wrapWaitClickXpath("//*[contains(@class,'multiselect dropdown-toggle')]");
			//Select random Group IP List
			try {
				Thread.sleep(1000);
				List<WebElement> lst10 = driver.findElements(By.xpath("//*[@type='checkbox']"));
				// Getting size of options available
				int size10 = lst10.size();
				// Generate a random number with in range
				int randnMumber10 = ThreadLocalRandom.current().nextInt(0, size10);//skip 0 = None Group List
				// Selecting random value
				lst10.get(randnMumber10).click();
				// driver.findElements(By.xpath("//*[@class='checkbox']")).get(0).click();
				System.out.println("Group List: " + lst10.get(randnMumber10).getText());
			} catch (Exception e) {
				System.out.println("No Group List");
			}
			//Select again to close selection
			wrapWaitClickXpath("//*[contains(@class,'multiselect dropdown-toggle')]");
			
			//Case 2: Install Type = YUM
			wrapWaitClickXpath("//option[@value='yuminstall']");
			wrapWaitSendKeysXpath("//*[@name='yuminstall']", randomUrl);

			// Submit information
			wrapWaitClickXpath("//*[@type='submit']");
			Thread.sleep(1000);
			// Compare Notification Result Message
			String actualTitle3 = wrapWaitGetTextXpath("//*[@id='modal-title-modalnotify']").toLowerCase();
			if (actualTitle3.contains("success")) {
				System.out.println("-> Data is inserted successfully/test_Deployment_New <-");
			} else if (actualTitle3.contains("exist")) {
				System.out.println("-> Same field was existed already. Please search again/test_Deployment_New <-");
			} else
				System.out.println("-> Not support 17/test_Deployment_New <-");

			// Click Ok button to close Message
			Thread.sleep(1000);
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(0).click();
			} catch (Exception e) {
			}
			try {
				driver.findElements(By.xpath("//*[contains(text(),'OK')]")).get(1).click();
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		//} catch (Exception e) {
		//	System.out.println("@Test(priority = 17) -> Failed");
		//}
	}
	
	@AfterTest
	public void tearDown() {

	}
}
