package priol_testCases;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import priom_operation.UIOperation_AutoSystem;
import prion_readFiles.ReadGuru99ExcelFile;


public class ExecuteTest_AutoSystem{
	public WebDriver driver;
	public WebDriverWait wait;
	public JavascriptExecutor js;

	@Parameters("browser")
	@BeforeClass
	public void setUp(String browserName) {
		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\BrowserDrivers\\chromedriver.exe");
			driver = new ChromeDriver();
//			js = (JavascriptExecutor) driver;
//			wait = new WebDriverWait(driver, 30);
//			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}

		else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\BrowserDrivers\\geckodriver.exe");
			driver = new FirefoxDriver();
		}

		else if (browserName.equalsIgnoreCase("Edge")) {
			// set path to Edge.exe
			// System.setProperty("webdriver.edge.driver", System.getProperty("user.dir")
			// +"\\BrowserDrivers\\msedgedriver.exe");
			// create Edge instance
			driver = new EdgeDriver();
		}
		else if (browserName.equalsIgnoreCase("ie")) {
			// set path to Edge.exe
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "\\BrowserDrivers\\IEDriverServer.exe");
			// create Edge instance
			driver = new InternetExplorerDriver();
		}
		else if (browserName.equalsIgnoreCase("opera")) {
			// set system property, so that we can access opera driver
			// System.setProperty("webdriver.opera.driver", System.getProperty("user.dir") +
			// "\\BrowserDrivers\\operadriver.exe");
			// it will open the opera browser
			OperaOptions options = new OperaOptions();
			options.setBinary(new File("C:\\Users\\LAP12778-local\\AppData\\Local\\Programs\\Opera\\60.0.3255.170_0\\opera.exe"));
			driver = new OperaDriver(options);
		}
		else if (browserName.equalsIgnoreCase("htmlunit")) {
			driver = new HtmlUnitDriver();
		}
		/*
		 * else if (browserName.equalsIgnoreCase("phantomjs")) { File file = new
		 * File(System.getProperty("user.dir") +
		 * "\\BrowserDrivers\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe"); //File file
		 * = new File("C:\\Program Files\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
		 * System.setProperty("phantomjs.binary.path", file.getAbsolutePath()); driver =
		 * new
		 * PhantomJSDriver("C:\\Program Files\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe"
		 * ); }
		 */
		else
			System.out.println("This driver is not supported.");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test (priority = 0, enabled = false)
	public void test_0_Login() throws Exception {
		// Step 1: call ReadGuru99ExcelFile.java, has POI script to read data from Excel
		ReadGuru99ExcelFile file = new ReadGuru99ExcelFile();
		//The driver will pass the data from Excel & Object Repository to UIOperation class
		UIOperation_AutoSystem operation = new UIOperation_AutoSystem(driver);
		// Read keyword sheet
		Sheet guru99Sheet = file.readExcel(System.getProperty("user.dir") + "\\", "AutoSystem_Key.xlsx",
				"Login");
		// Find number of rows in excel file
		int rowCount = guru99Sheet.getLastRowNum() - guru99Sheet.getFirstRowNum();
		// Create a loop over all the rows of excel file to read it
		for (int i = 1; i < rowCount + 1; i++) {
			// Loop over all the rows
			Row row = guru99Sheet.getRow(i);
			// Check if the first cell contain a value, if yes, it is new testcase
			if (row.getCell(0).toString().length() == 0) {
				// Print testcase detail on console
//				System.out.println(row.getCell(1).toString() + " ---a/ " + row.getCell(2).toString() + " ---b/ "
//						+ row.getCell(3).toString() + " ---c/ " + row.getCell(10).toString());
				// Call perform function to perform operation on UI
				operation.perform(row.getCell(1).toString(), row.getCell(2).toString(), row.getCell(3).toString(), row.getCell(4).toString(), row.getCell(5).toString(), row.getCell(6).toString(), row.getCell(7).toString(), row.getCell(8).toString(), row.getCell(9).toString(), row.getCell(10).toString(), row.getCell(11).toString(), row.getCell(12).toString(), row.getCell(13).toString(), row.getCell(14).toString(), row.getCell(15).toString(), row.getCell(16).toString(), row.getCell(17).toString(), row.getCell(18).toString(), row.getCell(19).toString(), row.getCell(20).toString(), row.getCell(21).toString(), row.getCell(22).toString(), row.getCell(23).toString(), row.getCell(24).toString(), row.getCell(25).toString(), row.getCell(26).toString(), row.getCell(27).toString(), row.getCell(28).toString(), row.getCell(29).toString(), row.getCell(30).toString());
			} else {
				// Print the new testcase name when it started
				System.out.println("Testcase -> " + row.getCell(0).toString() + ": Started");
			}
		}
	}	
	
	
	@Test (priority = 1, enabled = true)
	public void test_GAJ_41_Rerun_Task() throws Exception {
		// Step 1: call ReadGuru99ExcelFile.java, has POI script to read data from Excel
		ReadGuru99ExcelFile file = new ReadGuru99ExcelFile();
		//The driver will pass the data from Excel & Object Repository to UIOperation class
		UIOperation_AutoSystem operation = new UIOperation_AutoSystem(driver);
		// Read keyword sheet
		Sheet guru99Sheet = file.readExcel(System.getProperty("user.dir") + "\\", "AutoSystem_Key.xlsx",
				"GAJ-41");
		// Find number of rows in excel file
		int rowCount = guru99Sheet.getLastRowNum() - guru99Sheet.getFirstRowNum();
		// Create a loop over all the rows of excel file to read it
		for (int i = 1; i < rowCount + 1; i++) {
			// Loop over all the rows
			Row row = guru99Sheet.getRow(i);
			// Check if the first cell contain a value, if yes, it is new testcase
			if (row.getCell(0).toString().length() == 0) {
				// Print testcase detail on console
//				System.out.println(row.getCell(1).toString() + " ---a/ " + row.getCell(2).toString() + " ---b/ "
//						+ row.getCell(3).toString() + " ---c/ " + row.getCell(10).toString());
				// Call perform function to perform operation on UI
				operation.perform(row.getCell(1).toString(), row.getCell(2).toString(), row.getCell(3).toString(), row.getCell(4).toString(), row.getCell(5).toString(), row.getCell(6).toString(), row.getCell(7).toString(), row.getCell(8).toString(), row.getCell(9).toString(), row.getCell(10).toString(), row.getCell(11).toString(), row.getCell(12).toString(), row.getCell(13).toString(), row.getCell(14).toString(), row.getCell(15).toString(), row.getCell(16).toString(), row.getCell(17).toString(), row.getCell(18).toString(), row.getCell(19).toString(), row.getCell(20).toString(), row.getCell(21).toString(), row.getCell(22).toString(), row.getCell(23).toString(), row.getCell(24).toString(), row.getCell(25).toString(), row.getCell(26).toString(), row.getCell(27).toString(), row.getCell(28).toString(), row.getCell(29).toString(), row.getCell(30).toString());
			} else {
				// Print the new testcase name when it started
				System.out.println("Testcase -> " + row.getCell(0).toString() + ": Started");
			}
		}
	}	

	@Test (priority = 2, enabled = true)
	public void test_GAJ_57_New_Server_On_Staging_Task() throws Exception {
		// Step 1: call ReadGuru99ExcelFile.java, has POI script to read data from Excel
		ReadGuru99ExcelFile file = new ReadGuru99ExcelFile();
		//The driver will pass the data from Excel & Object Repository to UIOperation class
		UIOperation_AutoSystem operation = new UIOperation_AutoSystem(driver);
		// Read keyword sheet
		Sheet guru99Sheet = file.readExcel(System.getProperty("user.dir") + "\\", "AutoSystem_Key.xlsx",
				"GAJ-57");
		// Find number of rows in excel file
		int rowCount = guru99Sheet.getLastRowNum() - guru99Sheet.getFirstRowNum();
		// Create a loop over all the rows of excel file to read it
		for (int i = 1; i < rowCount + 1; i++) {
			// Loop over all the rows
			Row row = guru99Sheet.getRow(i);
			// Check if the first cell contain a value, if yes, it is new testcase
			if (row.getCell(0).toString().length() == 0) {
				// Print testcase detail on console
//				System.out.println(row.getCell(1).toString() + " ---a/ " + row.getCell(2).toString() + " ---b/ "
//						+ row.getCell(3).toString() + " ---c/ " + row.getCell(10).toString());
				// Call perform function to perform operation on UI
				operation.perform(row.getCell(1).toString(), row.getCell(2).toString(), row.getCell(3).toString(), row.getCell(4).toString(), row.getCell(5).toString(), row.getCell(6).toString(), row.getCell(7).toString(), row.getCell(8).toString(), row.getCell(9).toString(), row.getCell(10).toString(), row.getCell(11).toString(), row.getCell(12).toString(), row.getCell(13).toString(), row.getCell(14).toString(), row.getCell(15).toString(), row.getCell(16).toString(), row.getCell(17).toString(), row.getCell(18).toString(), row.getCell(19).toString(), row.getCell(20).toString(), row.getCell(21).toString(), row.getCell(22).toString(), row.getCell(23).toString(), row.getCell(24).toString(), row.getCell(25).toString(), row.getCell(26).toString(), row.getCell(27).toString(), row.getCell(28).toString(), row.getCell(29).toString(), row.getCell(30).toString());
			} else {
				// Print the new testcase name when it started
				System.out.println("Testcase -> " + row.getCell(0).toString() + ": Started");
			}
		}
	}
	
	
	@Test (priority = 3, enabled = true)
	public void test_GAJ_83_New_Service_On_Staging_task() throws Exception {
		// Step 1: call ReadGuru99ExcelFile.java, has POI script to read data from Excel
		ReadGuru99ExcelFile file = new ReadGuru99ExcelFile();
		//The driver will pass the data from Excel & Object Repository to UIOperation class
		UIOperation_AutoSystem operation = new UIOperation_AutoSystem(driver);
		// Read keyword sheet
		Sheet guru99Sheet = file.readExcel(System.getProperty("user.dir") + "\\", "AutoSystem_Key.xlsx",
				"GAJ-83");
		// Find number of rows in excel file
		int rowCount = guru99Sheet.getLastRowNum() - guru99Sheet.getFirstRowNum();
		// Create a loop over all the rows of excel file to read it
		for (int i = 1; i < rowCount + 1; i++) {
			// Loop over all the rows
			Row row = guru99Sheet.getRow(i);
			// Check if the first cell contain a value, if yes, it is new testcase
			if (row.getCell(0).toString().length() == 0) {
				// Print testcase detail on console
//				System.out.println(row.getCell(1).toString() + " ---a/ " + row.getCell(2).toString() + " ---b/ "
//						+ row.getCell(3).toString() + " ---c/ " + row.getCell(10).toString());
				// Call perform function to perform operation on UI
				operation.perform(row.getCell(1).toString(), row.getCell(2).toString(), row.getCell(3).toString(), row.getCell(4).toString(), row.getCell(5).toString(), row.getCell(6).toString(), row.getCell(7).toString(), row.getCell(8).toString(), row.getCell(9).toString(), row.getCell(10).toString(), row.getCell(11).toString(), row.getCell(12).toString(), row.getCell(13).toString(), row.getCell(14).toString(), row.getCell(15).toString(), row.getCell(16).toString(), row.getCell(17).toString(), row.getCell(18).toString(), row.getCell(19).toString(), row.getCell(20).toString(), row.getCell(21).toString(), row.getCell(22).toString(), row.getCell(23).toString(), row.getCell(24).toString(), row.getCell(25).toString(), row.getCell(26).toString(), row.getCell(27).toString(), row.getCell(28).toString(), row.getCell(29).toString(), row.getCell(30).toString());
			} else {
				// Print the new testcase name when it started
				System.out.println("Testcase -> " + row.getCell(0).toString() + ": Started");
			}
		}
	}
	
	
	@Test (priority = 4, enabled = false)
	public void test_GAJ_95_Maintenance_On_Staging_Task() throws Exception {
		// Step 1: call ReadGuru99ExcelFile.java, has POI script to read data from Excel
		ReadGuru99ExcelFile file = new ReadGuru99ExcelFile();
		//The driver will pass the data from Excel & Object Repository to UIOperation class
		UIOperation_AutoSystem operation = new UIOperation_AutoSystem(driver);
		// Read keyword sheet
		Sheet guru99Sheet = file.readExcel(System.getProperty("user.dir") + "\\", "AutoSystem_Key.xlsx",
				"GAJ-95");
		// Find number of rows in excel file
		int rowCount = guru99Sheet.getLastRowNum() - guru99Sheet.getFirstRowNum();
		// Create a loop over all the rows of excel file to read it
		for (int i = 1; i < rowCount + 1; i++) {
			// Loop over all the rows
			Row row = guru99Sheet.getRow(i);
			// Check if the first cell contain a value, if yes, it is new testcase
			if (row.getCell(0).toString().length() == 0) {
				// Print testcase detail on console
//				System.out.println(row.getCell(1).toString() + " ---a/ " + row.getCell(2).toString() + " ---b/ "
//						+ row.getCell(3).toString() + " ---c/ " + row.getCell(10).toString());
				// Call perform function to perform operation on UI
				operation.perform(row.getCell(1).toString(), row.getCell(2).toString(), row.getCell(3).toString(), row.getCell(4).toString(), row.getCell(5).toString(), row.getCell(6).toString(), row.getCell(7).toString(), row.getCell(8).toString(), row.getCell(9).toString(), row.getCell(10).toString(), row.getCell(11).toString(), row.getCell(12).toString(), row.getCell(13).toString(), row.getCell(14).toString(), row.getCell(15).toString(), row.getCell(16).toString(), row.getCell(17).toString(), row.getCell(18).toString(), row.getCell(19).toString(), row.getCell(20).toString(), row.getCell(21).toString(), row.getCell(22).toString(), row.getCell(23).toString(), row.getCell(24).toString(), row.getCell(25).toString(), row.getCell(26).toString(), row.getCell(27).toString(), row.getCell(28).toString(), row.getCell(29).toString(), row.getCell(30).toString());
			} else {
				// Print the new testcase name when it started
				System.out.println("Testcase -> " + row.getCell(0).toString() + ": Started");
			}
		}
	}
	
	@Test (priority = 5, enabled = true)//Need Change ID to run on Production
	public void test_GAJ_96_Maintenance_On_Production_Task() throws Exception {
		// Step 1: call ReadGuru99ExcelFile.java, has POI script to read data from Excel
		ReadGuru99ExcelFile file = new ReadGuru99ExcelFile();
		//The driver will pass the data from Excel & Object Repository to UIOperation class
		UIOperation_AutoSystem operation = new UIOperation_AutoSystem(driver);
		// Read keyword sheet
		Sheet guru99Sheet = file.readExcel(System.getProperty("user.dir") + "\\", "AutoSystem_Key.xlsx",
				"GAJ-96");
		// Find number of rows in excel file
		int rowCount = guru99Sheet.getLastRowNum() - guru99Sheet.getFirstRowNum();
		// Create a loop over all the rows of excel file to read it
		for (int i = 1; i < rowCount + 1; i++) {
			// Loop over all the rows
			Row row = guru99Sheet.getRow(i);
			// Check if the first cell contain a value, if yes, it is new testcase
			if (row.getCell(0).toString().length() == 0) {
				// Print testcase detail on console
//				System.out.println(row.getCell(1).toString() + " ---a/ " + row.getCell(2).toString() + " ---b/ "
//						+ row.getCell(3).toString() + " ---c/ " + row.getCell(10).toString());
				// Call perform function to perform operation on UI
				operation.perform(row.getCell(1).toString(), row.getCell(2).toString(), row.getCell(3).toString(), row.getCell(4).toString(), row.getCell(5).toString(), row.getCell(6).toString(), row.getCell(7).toString(), row.getCell(8).toString(), row.getCell(9).toString(), row.getCell(10).toString(), row.getCell(11).toString(), row.getCell(12).toString(), row.getCell(13).toString(), row.getCell(14).toString(), row.getCell(15).toString(), row.getCell(16).toString(), row.getCell(17).toString(), row.getCell(18).toString(), row.getCell(19).toString(), row.getCell(20).toString(), row.getCell(21).toString(), row.getCell(22).toString(), row.getCell(23).toString(), row.getCell(24).toString(), row.getCell(25).toString(), row.getCell(26).toString(), row.getCell(27).toString(), row.getCell(28).toString(), row.getCell(29).toString(), row.getCell(30).toString());
			} else {
				// Print the new testcase name when it started
				System.out.println("Testcase -> " + row.getCell(0).toString() + ": Started");
			}
		}
	}
	
	@Test (priority = 6, enabled = false)
	public void test_GAJ_100_Uppatch_On_Production_Task() throws Exception {
		// Step 1: call ReadGuru99ExcelFile.java, has POI script to read data from Excel
		ReadGuru99ExcelFile file = new ReadGuru99ExcelFile();
		//The driver will pass the data from Excel & Object Repository to UIOperation class
		UIOperation_AutoSystem operation = new UIOperation_AutoSystem(driver);
		// Read keyword sheet
		Sheet guru99Sheet = file.readExcel(System.getProperty("user.dir") + "\\", "AutoSystem_Key.xlsx",
				"GAJ-100");
		// Find number of rows in excel file
		int rowCount = guru99Sheet.getLastRowNum() - guru99Sheet.getFirstRowNum();
		// Create a loop over all the rows of excel file to read it
		for (int i = 1; i < rowCount + 1; i++) {
			// Loop over all the rows
			Row row = guru99Sheet.getRow(i);
			// Check if the first cell contain a value, if yes, it is new testcase
			if (row.getCell(0).toString().length() == 0) {
				// Print testcase detail on console
//				System.out.println(row.getCell(1).toString() + " ---a/ " + row.getCell(2).toString() + " ---b/ "
//						+ row.getCell(3).toString() + " ---c/ " + row.getCell(10).toString());
				// Call perform function to perform operation on UI
				operation.perform(row.getCell(1).toString(), row.getCell(2).toString(), row.getCell(3).toString(), row.getCell(4).toString(), row.getCell(5).toString(), row.getCell(6).toString(), row.getCell(7).toString(), row.getCell(8).toString(), row.getCell(9).toString(), row.getCell(10).toString(), row.getCell(11).toString(), row.getCell(12).toString(), row.getCell(13).toString(), row.getCell(14).toString(), row.getCell(15).toString(), row.getCell(16).toString(), row.getCell(17).toString(), row.getCell(18).toString(), row.getCell(19).toString(), row.getCell(20).toString(), row.getCell(21).toString(), row.getCell(22).toString(), row.getCell(23).toString(), row.getCell(24).toString(), row.getCell(25).toString(), row.getCell(26).toString(), row.getCell(27).toString(), row.getCell(28).toString(), row.getCell(29).toString(), row.getCell(30).toString());
			} else {
				// Print the new testcase name when it started
				System.out.println("Testcase -> " + row.getCell(0).toString() + ": Started");
			}
		}
	}
	
	@Test (priority = 7, enabled = true)
	public void test_GAJ_101_Uppatch_On_Staging_Task() throws Exception {
		// Step 1: call ReadGuru99ExcelFile.java, has POI script to read data from Excel
		ReadGuru99ExcelFile file = new ReadGuru99ExcelFile();
		//The driver will pass the data from Excel & Object Repository to UIOperation class
		UIOperation_AutoSystem operation = new UIOperation_AutoSystem(driver);
		// Read keyword sheet
		Sheet guru99Sheet = file.readExcel(System.getProperty("user.dir") + "\\", "AutoSystem_Key.xlsx",
				"GAJ-101");
		// Find number of rows in excel file
		int rowCount = guru99Sheet.getLastRowNum() - guru99Sheet.getFirstRowNum();
		// Create a loop over all the rows of excel file to read it
		for (int i = 1; i < rowCount + 1; i++) {
			// Loop over all the rows
			Row row = guru99Sheet.getRow(i);
			// Check if the first cell contain a value, if yes, it is new testcase
			if (row.getCell(0).toString().length() == 0) {
				// Print testcase detail on console
//				System.out.println(row.getCell(1).toString() + " ---a/ " + row.getCell(2).toString() + " ---b/ "
//						+ row.getCell(3).toString() + " ---c/ " + row.getCell(10).toString());
				// Call perform function to perform operation on UI
				operation.perform(row.getCell(1).toString(), row.getCell(2).toString(), row.getCell(3).toString(), row.getCell(4).toString(), row.getCell(5).toString(), row.getCell(6).toString(), row.getCell(7).toString(), row.getCell(8).toString(), row.getCell(9).toString(), row.getCell(10).toString(), row.getCell(11).toString(), row.getCell(12).toString(), row.getCell(13).toString(), row.getCell(14).toString(), row.getCell(15).toString(), row.getCell(16).toString(), row.getCell(17).toString(), row.getCell(18).toString(), row.getCell(19).toString(), row.getCell(20).toString(), row.getCell(21).toString(), row.getCell(22).toString(), row.getCell(23).toString(), row.getCell(24).toString(), row.getCell(25).toString(), row.getCell(26).toString(), row.getCell(27).toString(), row.getCell(28).toString(), row.getCell(29).toString(), row.getCell(30).toString());
			} else {
				// Print the new testcase name when it started
				System.out.println("Testcase -> " + row.getCell(0).toString() + ": Started");
			}
		}
	}
	
	
	@Test (priority = 8, enabled = false)
	public void test_GAJ_105_New_Server_On_Production_Task() throws Exception {
		// Step 1: call ReadGuru99ExcelFile.java, has POI script to read data from Excel
		ReadGuru99ExcelFile file = new ReadGuru99ExcelFile();
		//The driver will pass the data from Excel & Object Repository to UIOperation class
		UIOperation_AutoSystem operation = new UIOperation_AutoSystem(driver);
		// Read keyword sheet
		Sheet guru99Sheet = file.readExcel(System.getProperty("user.dir") + "\\", "AutoSystem_Key.xlsx",
				"GAJ-105");
		// Find number of rows in excel file
		int rowCount = guru99Sheet.getLastRowNum() - guru99Sheet.getFirstRowNum();
		// Create a loop over all the rows of excel file to read it
		for (int i = 1; i < rowCount + 1; i++) {
			// Loop over all the rows
			Row row = guru99Sheet.getRow(i);
			// Check if the first cell contain a value, if yes, it is new testcase
			if (row.getCell(0).toString().length() == 0) {
				// Print testcase detail on console
//				System.out.println(row.getCell(1).toString() + " ---a/ " + row.getCell(2).toString() + " ---b/ "
//						+ row.getCell(3).toString() + " ---c/ " + row.getCell(10).toString());
				// Call perform function to perform operation on UI
				operation.perform(row.getCell(1).toString(), row.getCell(2).toString(), row.getCell(3).toString(), row.getCell(4).toString(), row.getCell(5).toString(), row.getCell(6).toString(), row.getCell(7).toString(), row.getCell(8).toString(), row.getCell(9).toString(), row.getCell(10).toString(), row.getCell(11).toString(), row.getCell(12).toString(), row.getCell(13).toString(), row.getCell(14).toString(), row.getCell(15).toString(), row.getCell(16).toString(), row.getCell(17).toString(), row.getCell(18).toString(), row.getCell(19).toString(), row.getCell(20).toString(), row.getCell(21).toString(), row.getCell(22).toString(), row.getCell(23).toString(), row.getCell(24).toString(), row.getCell(25).toString(), row.getCell(26).toString(), row.getCell(27).toString(), row.getCell(28).toString(), row.getCell(29).toString(), row.getCell(30).toString());
			} else {
				// Print the new testcase name when it started
				System.out.println("Testcase -> " + row.getCell(0).toString() + ": Started");
			}
		}
	}
	
	@Test (priority = 9, enabled = true)
	public void test_GAJ_127_User_In_Automation_Cannot_Run_BPWPL_Task() throws Exception {
		// Step 1: call ReadGuru99ExcelFile.java, has POI script to read data from Excel
		ReadGuru99ExcelFile file = new ReadGuru99ExcelFile();
		//The driver will pass the data from Excel & Object Repository to UIOperation class
		UIOperation_AutoSystem operation = new UIOperation_AutoSystem(driver);
		// Read keyword sheet
		Sheet guru99Sheet = file.readExcel(System.getProperty("user.dir") + "\\", "AutoSystem_Key.xlsx",
				"GAJ-127");
		// Find number of rows in excel file
		int rowCount = guru99Sheet.getLastRowNum() - guru99Sheet.getFirstRowNum();
		// Create a loop over all the rows of excel file to read it
		for (int i = 1; i < rowCount + 1; i++) {
			// Loop over all the rows
			Row row = guru99Sheet.getRow(i);
			// Check if the first cell contain a value, if yes, it is new testcase
			if (row.getCell(0).toString().length() == 0) {
				// Print testcase detail on console
//				System.out.println(row.getCell(1).toString() + " ---a/ " + row.getCell(2).toString() + " ---b/ "
//						+ row.getCell(3).toString() + " ---c/ " + row.getCell(10).toString());
				// Call perform function to perform operation on UI
				operation.perform(row.getCell(1).toString(), row.getCell(2).toString(), row.getCell(3).toString(), row.getCell(4).toString(), row.getCell(5).toString(), row.getCell(6).toString(), row.getCell(7).toString(), row.getCell(8).toString(), row.getCell(9).toString(), row.getCell(10).toString(), row.getCell(11).toString(), row.getCell(12).toString(), row.getCell(13).toString(), row.getCell(14).toString(), row.getCell(15).toString(), row.getCell(16).toString(), row.getCell(17).toString(), row.getCell(18).toString(), row.getCell(19).toString(), row.getCell(20).toString(), row.getCell(21).toString(), row.getCell(22).toString(), row.getCell(23).toString(), row.getCell(24).toString(), row.getCell(25).toString(), row.getCell(26).toString(), row.getCell(27).toString(), row.getCell(28).toString(), row.getCell(29).toString(), row.getCell(30).toString());
			} else {
				// Print the new testcase name when it started
				System.out.println("Testcase -> " + row.getCell(0).toString() + ": Started");
			}
		}
	}	
	
	@Test (priority = 10, enabled = true)
	public void test_GAJ_128_User_Cannot_See_Admin_Section() throws Exception {
		// Step 1: call ReadGuru99ExcelFile.java, has POI script to read data from Excel
		ReadGuru99ExcelFile file = new ReadGuru99ExcelFile();
		//The driver will pass the data from Excel & Object Repository to UIOperation class
		UIOperation_AutoSystem operation = new UIOperation_AutoSystem(driver);
		// Read keyword sheet
		Sheet guru99Sheet = file.readExcel(System.getProperty("user.dir") + "\\", "AutoSystem_Key.xlsx",
				"GAJ-128");
		// Find number of rows in excel file
		int rowCount = guru99Sheet.getLastRowNum() - guru99Sheet.getFirstRowNum();
		// Create a loop over all the rows of excel file to read it
		for (int i = 1; i < rowCount + 1; i++) {
			// Loop over all the rows
			Row row = guru99Sheet.getRow(i);
			// Check if the first cell contain a value, if yes, it is new testcase
			if (row.getCell(0).toString().length() == 0) {
				// Print testcase detail on console
//				System.out.println(row.getCell(1).toString() + " ---a/ " + row.getCell(2).toString() + " ---b/ "
//						+ row.getCell(3).toString() + " ---c/ " + row.getCell(10).toString());
				// Call perform function to perform operation on UI
				operation.perform(row.getCell(1).toString(), row.getCell(2).toString(), row.getCell(3).toString(), row.getCell(4).toString(), row.getCell(5).toString(), row.getCell(6).toString(), row.getCell(7).toString(), row.getCell(8).toString(), row.getCell(9).toString(), row.getCell(10).toString(), row.getCell(11).toString(), row.getCell(12).toString(), row.getCell(13).toString(), row.getCell(14).toString(), row.getCell(15).toString(), row.getCell(16).toString(), row.getCell(17).toString(), row.getCell(18).toString(), row.getCell(19).toString(), row.getCell(20).toString(), row.getCell(21).toString(), row.getCell(22).toString(), row.getCell(23).toString(), row.getCell(24).toString(), row.getCell(25).toString(), row.getCell(26).toString(), row.getCell(27).toString(), row.getCell(28).toString(), row.getCell(29).toString(), row.getCell(30).toString());
			} else {
				// Print the new testcase name when it started
				System.out.println("Testcase -> " + row.getCell(0).toString() + ": Started");
			}
		}
	}
	
	
	@AfterClass
	public void tearDown() {
		driver.quit();
	}
	
}
