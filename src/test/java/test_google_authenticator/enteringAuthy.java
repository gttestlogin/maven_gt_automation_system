package test_google_authenticator;

import org.jboss.aerogear.security.otp.Totp;

public class enteringAuthy {

	public static void main(String[] args) {
		try {
			String otpKeyStr = "6NXL EEI7 BZPG BXYR"; // <- this 2FA secret key.

			Totp totp = new Totp(otpKeyStr);
			String twoFactorCode = totp.now(); // <- got 2FA coed at this time!
			System.out.println("twoFactorCode:" +twoFactorCode);
		} catch (Exception e) {
			System.out.println("cannot get Google Authenticator at this time");
		}

	}

}
